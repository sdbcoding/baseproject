﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchThing : MonoBehaviour
{
    public GameObject punchObj;
    public float punchDuration;
    public void Punch()
    {
        punchObj.SetActive(true);
        Invoke("ClosePunch", punchDuration);
    }

    void ClosePunch()
    {
        punchObj.SetActive(false);
    }
}
