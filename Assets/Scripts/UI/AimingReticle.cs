﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimingReticle : MonoBehaviour
{
    public RectTransform rect;
    public FloatReference distanceFromCenter;
    public Vector3Variable positionOfAimNormalized;
    public Vector3Variable playerposition;
    Camera mainCamera;
    //Vector3 screenMiddle;
    // Start is called before the first frame update

    Vector3 newPosition;
    void Start()
    {
        if (rect == null)
        {
            rect = GetComponent<RectTransform>();
        }
        //screenMiddle = new Vector3(Screen.width / 2, Screen.height / 2,0);
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(mainCamera.WorldToScreenPoint(playerTransform.position));
        newPosition = mainCamera.WorldToScreenPoint(playerposition.Value)  + (positionOfAimNormalized.Value * distanceFromCenter.Value); 
        rect.position = newPosition;
        rect.up = (positionOfAimNormalized.Value);
        //Debug.Log("NewPos: " + newPosition + " positionOf: " + positionOfAimNormalized.Value + " combined: " + (newPosition + positionOfAimNormalized.Value));
    }
}
