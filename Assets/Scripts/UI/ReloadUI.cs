﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReloadUI : OnUpdate
{
    public GameObject backgroundImage;
    public Image progressImage;
    public FloatReference currentTime;
    public FloatReference reloadTime;
    public void StartUI()
    {
        backgroundImage.SetActive(true);
        progressImage.fillAmount = 0;

    }

    public override void UpdateEventRaised(float deltaTime)
    {
        if (currentTime != 0)
        {
            progressImage.fillAmount = currentTime / reloadTime;
        }
    }

    public void HideUI()
    {
        backgroundImage.SetActive(false);
    }
}
