﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashReloadedGrow : MonoBehaviour {

	RectTransform rectTransform;
	Image image;
	bool isRunning;
	Vector2 startSizeDelta;
	Color startColor;

	float currentTime;
	float timeToReach = 0.5f;
	void Start()
	{
		rectTransform = GetComponent<RectTransform>();
		image= GetComponent<Image>();
		startSizeDelta = rectTransform.sizeDelta;
		startColor = image.color;
	}
	public void Activate()
	{
		gameObject.SetActive(true);
		isRunning = true;
	}
	void ResetValues()
	{
		rectTransform.sizeDelta = startSizeDelta;
		image.color = startColor;
		currentTime = 0;
		gameObject.SetActive(false);
	}
	void EndActivate()
	{
		ResetValues();
		isRunning = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isRunning) {return;}
		rectTransform.sizeDelta *= 1.05f;
		Color newColor = image.color;
		newColor.a *= 0.95f;
		image.color = newColor;
		currentTime += Time.deltaTime;
		if (currentTime > timeToReach)
		{
			EndActivate();
		}
	}
}
