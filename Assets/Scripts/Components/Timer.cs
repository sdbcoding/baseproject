﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour {

	public bool runAtStart = false;
	public float timeToReach;
	public UnityEvent response;
	float currentTime = 0;
	bool isRunning = false;
	// Use this for initialization
	void Start () {
		if (runAtStart)
		{
			StartTimer();
		}
	}
	
	void Update()
	{
		if (isRunning)
		{
			currentTime += Time.deltaTime;
			if (currentTime > timeToReach)
			{
				response.Invoke();
				Destroy(this);
				return;
			}
		}
	}
	public void StartTimer()
	{
		isRunning = true;
	}
	public void StopTimer()
	{
		isRunning = false;
	}
}
