﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesireMoveTowardsPlayer : MonoBehaviour,DesireInterface
{
    /*  Stuff to improve
     *  Maybe add desire if weapon is melee
     */

    public Memory memory;
    public RightHand rightHand;
    public Health health;
    public Legs legs;
    public Head head;
    public ThingSet playerSet;

    float desireLevel;

    float lastPlayerLocationHaveBeenChecked = -10;

    float maxTimeBeforePlayerLocationIsIrrelevant = 15;
    float minProcentageDistanceToPlayerAccepted = 0.3f;

    float playerIsTooFarAway = 50;

    float timeWhereTakingDamageIsrelevant = 3;
    float tookDamageRecently = -15;

    float minProcentHealthBeforeRetreat = 0.2f;
    float belowHealthThreshold = -30;



    //temp

    public bool DesireActivity()
    {
        Debug.Log("Running desire: Move towards player");
        if (!head.IsFocusingAt(playerSet.GetItemsAtIndex(0).gameObject))
        {
            head.FocusOnObject(playerSet.GetItemsAtIndex(0).gameObject);
        }
        legs.SetDestination(memory.LastKnownThreatPosition);
        return true;
    }

    public float DesireLevel(float deltaSinceLastCheck)
    {

        //if (memory.timeSinceThreatWasSpotted > maxTimeBeforePlayerLocationIsIrrelevant)
        //{
        //    //Player location is no longer relevant, no point in moving to his location.
        //    return -100;
        //}
        //if (memory.distanceToThreat / rightHand.weaponInHand.weaponType.relevantRange < minProcentageDistanceToPlayerAccepted && memory.isThreatVisible)
        //{
        //    //Player is close enough and visible
        //    return -100;
        //}
        //desireLevel = 0;
        //if (memory.checkedLastKnowThreatPosition)
        //{
        //    desireLevel += lastPlayerLocationHaveBeenChecked;
        //}
        //if (memory.distanceToThreat > rightHand.weaponInHand.weaponType.relevantRange)
        //{
        //    //Player is too far away
        //    desireLevel += playerIsTooFarAway;
        //}
        if (health.GetCurrentHealth() < health.startingHealth)
        {
            if (health.HealthProcentage < minProcentHealthBeforeRetreat)
            {
                desireLevel += belowHealthThreshold;
            }
            if (health.lastDamageTime < timeWhereTakingDamageIsrelevant)
            {
                desireLevel += tookDamageRecently;
            }
        }
        return desireLevel;
    }

    
}
