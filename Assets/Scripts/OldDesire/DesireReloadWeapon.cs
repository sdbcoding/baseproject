﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesireReloadWeapon : MonoBehaviour,DesireInterface
{
    public RightHand righthand;
    public ReloadSystem reloadSystem;
    public Memory memory;

    float playerIsVisible = -10;
    float playerIsNotVisible = 10;

    float outOfBullets = 30;
    float desireDependingOnbullets = 20;

    //temp
    float desireLevel;
    public bool DesireActivity()
    {
        reloadSystem.ReloadCommand();
        return true;
    }

    public float DesireLevel(float deltaSinceLastCheck)
    {
        if (righthand.weaponInHand == null)
        {
            //No weapon to reload
            return -100;
        }
        if (righthand.numberOfBulletsInMagazine == righthand.weaponInHand.magazine.capacity)
        {
            //Already fully loaded
            return -100;
        }
        desireLevel = 0;

        //if(memory.isThreatVisible)
        //{
        //    desireLevel += playerIsVisible;
        //} else
        //{
        //    desireLevel += playerIsNotVisible;
        //}

        if(righthand.numberOfBulletsInMagazine == 0)
        {
            desireLevel += outOfBullets;
        } else
        {
            desireLevel += desireDependingOnbullets * (1 - (righthand.numberOfBulletsInMagazine / righthand.weaponInHand.magazine.capacity));
        }


        return desireLevel;
    }

    
}
