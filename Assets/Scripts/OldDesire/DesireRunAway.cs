﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesireRunAway : MonoBehaviour,DesireInterface
{
    public Health health;
    public Memory memory;
    public AIState aiState;
    public Legs legs;
    public ThingSet playerSet;
    public ThingSet hidingSpots;
    VisibleCharacterInformation playerInformation;
    bool isRunningAway;
    float timeBeforeStillpanickingIsIrrelevant = 20;

    float maxCareForHealth = 0.5f;
    float maxDesireFromLowHealth = 50;

    float playerIsvisibleWithGun = 10;
    float playerHasNoGun = -30;

    float alreadyRunningAway = 80;
    

    //temp
    float desireLevel;
    float angle;
    float distance;
    int runAwayIndex;
    

    public bool DesireActivity()
    {
        if (aiState.CurrentState == AIState.State.Panicked)
        {
            distance = Vector3.Distance(transform.position, hidingSpots.GetItemsAtIndex(runAwayIndex).transform.position);
            if(distance < 2.0f)
            {
                aiState.CurrentState = AIState.State.Normal;
            }
            return true;
        }
        runAwayIndex = 0;
        for (int i = 0; i < hidingSpots.GetItemListSize(); i++)
        {
            angle = Vector3.Angle(hidingSpots.GetItemsAtIndex(i).transform.position, playerSet.GetItemsAtIndex(0).transform.position);
            if (angle > 90)
            {
                runAwayIndex = i;
            }
        }
        legs.SetDestination(hidingSpots.GetItemsAtIndex(runAwayIndex).transform.position);
        isRunningAway = true;
        return true;
    }

    public float DesireLevel(float deltaSinceLastCheck)
    {
        desireLevel = 0;
        if (health.HealthProcentage == 1)
        {
            return -100;
        }
        if (health.HealthProcentage > maxCareForHealth)
        {
            desireLevel += maxDesireFromLowHealth * (1 - (health.HealthProcentage / maxCareForHealth));
        }
        if (playerInformation.GetWeaponInfo() == null)
        {
            desireLevel += playerHasNoGun;
        } else
        {
            if (memory.IsThreatVisible)
            {
                desireLevel += playerIsvisibleWithGun;
            }
        }

        if (aiState.CurrentState == AIState.State.Panicked )
        {
            if (aiState.lastTimePanicked < timeBeforeStillpanickingIsIrrelevant)
            {
                desireLevel += alreadyRunningAway * (1-(aiState.lastTimePanicked / timeBeforeStillpanickingIsIrrelevant));
            }
        }

        return desireLevel;
    }

    
}
