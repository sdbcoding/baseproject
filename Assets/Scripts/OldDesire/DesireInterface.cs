﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface DesireInterface
{
    float DesireLevel(float deltaSinceLastCheck);
    bool DesireActivity();
}
