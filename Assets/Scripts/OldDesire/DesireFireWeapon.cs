﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ItemNamespace;

public class DesireFireWeapon : MonoBehaviour,DesireInterface
{
    public RightHand rightHand;
    public Head head;
    public Memory memory;
    public Stats stats;
    public ThingSet playerSet;

    float minAcceptedShootingAngleDiff = 20;
    

    //values for test
    float outofbullets = -20;
    bool triedFireringLastCheckButWasOutOfBullets = false;
    float isNotCockeed = -30;
    bool triedFireringLastCheckButWasntCocked = false;

    //PlayerVisibility values
    float playerIsVisible = 80;
    float acceptableTimeSincePlayerWasSpottedBeforeNoLongerPositive = 7;
    float maxPointFromSincePlayerWasSeenTime = 40;
    float maxTimeBeforeTimeSincePlayerWasSpottedIsIrrelevant = 14;
    float maxMinusIfPlayerPositionIsIrrelevant = -60;

    //PlayerDistanceStuff
    float playerOutOfRangeValue = -60;
    float maxValueForPlayerDistance = 60;

    //Stats stuff
    float maxFromBadAccuracy = -100;
    float thresholdForAccuracy = 80;

    //Temp variables
    float desireLevel;
    public bool DesireActivity()
    {
        //Debug.Log("Memory.AngleToPlayer: " + memory.angleToPlayer + " stats: " + (100 - stats.Accuracy));
        //if (memory.angleToThreat > minAcceptedShootingAngleDiff)
        //{
        //    if (head.IsFocusingAt(playerSet.GetItemsAtIndex(0).gameObject))
        //    {
        //        return false;
        //    }
        //    head.FocusOnObject(playerSet.GetItemsAtIndex(0).gameObject);
        //    return false;
        //}
        //if (rightHand.numberOfBulletsInMagazine == 0)
        //{
        //    //Got no bullets, but still tries to fire. Don't want that to happen twice in a row
        //    triedFireringLastCheckButWasOutOfBullets = true;
        //} else if (!rightHand.cockingSystem.GetIsWeaponCocked())
        //{
        //    triedFireringLastCheckButWasntCocked = true;
        //}
        //rightHand.FireWeaponCommand();
        return true;
    }

    public float DesireLevel(float deltaSinceLastCheck)
    {

        //Check if there is any weapon
        //if (rightHand.weaponInHand == null || memory.checkedLastKnowThreatPosition)
        //{
        //    //Got no weapon, no reason to fire
        //    //No idea where the player is
        //    return -100;
        //}

        desireLevel = 0;
        //Check for bullets in chamber
        if (rightHand.numberOfBulletsInMagazine == 0)
        {
            if (triedFireringLastCheckButWasOutOfBullets)
            {
                //Found out last time that there was no bullets in chamber
                triedFireringLastCheckButWasOutOfBullets = false;
                return -100;
            }

            //Got no bullets, less chance of trying to fire
            desireLevel += outofbullets;
        } else
        {
            triedFireringLastCheckButWasOutOfBullets = false;
        }

        //if (!rightHand.cockingSystem.GetIsWeaponCocked())
        //{
        //    if (triedFireringLastCheckButWasntCocked)
        //    {
        //        triedFireringLastCheckButWasOutOfBullets = false;
        //        return -100;
        //    }

        //    desireLevel += isNotCockeed;

        //} else
        //{
        //    triedFireringLastCheckButWasntCocked = false;
        //}

        //Check for knowledge of player
        //if (memory.isThreatVisible)
        //{
        //    //Player is visible and there is a clear line of sight
        //    desireLevel += playerIsVisible;
        //} else
        //{
        //    if (memory.timeSinceThreatWasSpotted < acceptableTimeSincePlayerWasSpottedBeforeNoLongerPositive)
        //    {
        //        desireLevel += maxPointFromSincePlayerWasSeenTime * (memory.timeSinceThreatWasSpotted / acceptableTimeSincePlayerWasSpottedBeforeNoLongerPositive);
        //    } else
        //    {
        //        if (memory.timeSinceThreatWasSpotted > maxTimeBeforeTimeSincePlayerWasSpottedIsIrrelevant)
        //        {
        //            desireLevel -= maxMinusIfPlayerPositionIsIrrelevant;
        //        } else
        //        {
        //            desireLevel -= maxMinusIfPlayerPositionIsIrrelevant * ((memory.timeSinceThreatWasSpotted - acceptableTimeSincePlayerWasSpottedBeforeNoLongerPositive) / (maxTimeBeforeTimeSincePlayerWasSpottedIsIrrelevant - acceptableTimeSincePlayerWasSpottedBeforeNoLongerPositive));
        //        }
        //    }
        //}

        ////Check for distance to player
        //if (memory.distanceToThreat < rightHand.weaponInHand.weaponType.relevantRange)
        //{
        //    desireLevel += maxValueForPlayerDistance * (1 -(memory.distanceToThreat / rightHand.weaponInHand.weaponType.relevantRange));

        //} else
        //{
        //    desireLevel += playerOutOfRangeValue;
        //}

        //Check stats

        if (stats.Accuracy < thresholdForAccuracy)
        {
            desireLevel += maxFromBadAccuracy * (1 - (stats.Accuracy / thresholdForAccuracy));
            //Debug.Log("Impact from accuracy: " + (maxFromBadAccuracy * (1 - (stats.Accuracy / thresholdForAccuracy))));
        }

        return desireLevel;
    }

    
}
