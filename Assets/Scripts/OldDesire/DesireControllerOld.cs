﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class DesireControllerOld : OnUpdate
{
    public Memory memory;
    public AIState aiState;
    public Transform desireModule;
    DesireInterface[] desires;
    float[] desireValues;
    int lastChosenDesire;

    public FloatReference timeBetweenChecks;
    float deltaSinceLastCheck;



    //Temp
    int tempChosenDesire;
    float highestDesireValue;
    float[] printableDesires;
    DesireInterface[] pritableDesireInterface;

    private void Start()
    {
        desires = desireModule.GetComponents<DesireInterface>();
        desireValues = new float[desires.Length];
    }

    public override void UpdateEventRaised(float deltaTime)
    {
        if (aiState.CurrentFrameOfMind != AIState.FrameOfMind.Combat) { return; }
        deltaSinceLastCheck += deltaTime;
        if (deltaSinceLastCheck > timeBetweenChecks)
        {
            SetupForDesireCheck();
            GetDesireLevels();
            FindHighestDesireLevel();
            ActivateHighestDesireLevel();
            //PrintDesires();

            deltaSinceLastCheck = 0;
        }
    }
    void PrintDesires()
    {
        printableDesires = desireValues;
        pritableDesireInterface = desires;
        /*
        for (int i = 0; i < printableDesires.Length; i++)
        {
            Debug.Log(i + ": " + desires[i].GetType() + " with: " + desireValues[i]);
        }
        */
        //Array.Sort(printableDesires, (a, b) => a.CompareTo(b));
        Array.Sort(desireValues, pritableDesireInterface);
        Array.Sort(desireValues, printableDesires);
        for (int i = 0; i < printableDesires.Length; i++)
        {
            Debug.Log(i + ": " + pritableDesireInterface[i].GetType() + " with: " + printableDesires[i]);
        }

    }
    void SetupForDesireCheck()
    {
        //memory.ReadyForDesireChecks();
    }

    void GetDesireLevels()
    {
        for (int i = 0; i < desires.Length; i++)
        {
            desireValues[i] = desires[i].DesireLevel(deltaSinceLastCheck);
        }
    }
    void FindHighestDesireLevel()
    {
        tempChosenDesire = 0;
        highestDesireValue = desireValues[0];
        for (int i = 1; i < desires.Length; i++)
        {
            if (desireValues[i] > highestDesireValue)
            {
                highestDesireValue = desireValues[i];
                tempChosenDesire = i;
            }
        }
        lastChosenDesire = tempChosenDesire;
    }
    void ActivateHighestDesireLevel()
    {
        desires[lastChosenDesire].DesireActivity();
    }
}
