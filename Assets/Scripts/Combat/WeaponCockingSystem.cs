﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCockingSystem : MonoBehaviour
{
    bool isWeaponCocked = true;
    
    
    public bool IsWeaponCocked { get => isWeaponCocked; set => isWeaponCocked = value; }

    public void CockWeapon()
    {
        IsWeaponCocked = true;
    }
    public void UnCockWeapon()
    {

    }

}
