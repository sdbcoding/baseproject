﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBullet : MonoBehaviour
{
    public GameObject bullet;
    public GameObjectSet bullets;
    public FloatReference distanceFromGunCenterToGunEnd;
    public FloatReference yValue;


    Vector3 newPosition;
    Vector3 newDirection;

    private void Start()
    {
        //Debug.Log(bullets.GetItemListSize());
        if (bullets.GetItemListSize() == 0)
        {
            AddNewBulletToSet();
        }
    }

    void AddNewBulletToSet()
    {
        GameObject temp = Instantiate(bullet);
        temp.SetActive(false);
        bullets.Add(temp);
    }
    

    public GameObject SpawnBulletCommand(float angleFromForward)
    {
        for (int i = 0; i < bullets.GetItemListSize(); i++)
        {
            if (!bullets.GetItemsAtIndex(i).activeSelf)
            {
                bullets.GetItemsAtIndex(i).gameObject.SetActive(true);
                ActivateBullet(bullets.GetItemsAtIndex(i),angleFromForward);
                return bullets.GetItemsAtIndex(i);
            }
        }
        AddNewBulletToSet();
        ActivateBullet(bullets.GetItemsAtIndex(bullets.GetItemListSize() - 1),angleFromForward);
        return bullets.GetItemsAtIndex(bullets.GetItemListSize() - 1);
    }
    void ActivateBullet(GameObject bullet, float angleFromForward)
    {
        //Todo add the spread to bullets
        bullet.gameObject.SetActive(true);
        newDirection = transform.up;
        
        //translate it so it goes in the right direction
        newDirection = Quaternion.Euler(0, angleFromForward, 0) * newDirection;
        bullet.transform.forward = newDirection;
        newDirection *= distanceFromGunCenterToGunEnd;
        newPosition = transform.position + newDirection;
        newPosition.y = yValue.Value;
        bullet.transform.position = newPosition;
    }

    


}
