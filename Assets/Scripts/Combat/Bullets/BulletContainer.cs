﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WeaponNamespace;
using ItemNamespace;

public class BulletContainer : MonoBehaviour
{
    public CockingMechanism cockingMechanism;
    public Bullet bullet;
    
    public void SetupBullet(Bullet newBullet,CockingMechanism newCockingMechanism)
    {
        bullet = newBullet;
        cockingMechanism = newCockingMechanism;
    }
}
