﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollision : MonoBehaviour
{
    public BulletContainer bulletContainer;
    [SerializeField]
    LayerMaskVariable layerMaskVariable;
    [SerializeField]
    FloatReference damageAmountVariable;

    public ThingSet enemies;

    LayerMask layerMask;

    private void OnEnable()
    {
        layerMask = layerMaskVariable.GetValue();
        //Debug.Log(damageAmount);
    }
    

    public void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("objectLayer: " + collision.gameObject.layer + " layerMask: " + layerMask);
        if(layerMask == (layerMask | (1 << collision.gameObject.layer)))
        {
            Health health = collision.gameObject.GetComponent<Health>();
            if (health != null)
            {
                //Debug.Log("Doing damage");
                health.TakeDamage(bulletContainer.bullet.damageAmount);
            }
            DisableBullet();
            return;

            Thing[] things = collision.gameObject.GetComponents<Thing>();
            for (int i = 0; i < things.Length; i++)
            {
                if (ThingSet.Equals(things[i].RunTimeSet,enemies))
                {
                    //We got an enemy, do damage to him!
                    collision.gameObject.GetComponent<EnemyHealth>();
                    return;
                }

            }
            //We just had something else we should collide with
            gameObject.SetActive(false);
        } 

    }

    void DisableBullet()
    {
        gameObject.SetActive(false);
    }

    
}
