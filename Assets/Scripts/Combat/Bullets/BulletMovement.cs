﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : OnUpdate
{
    public BulletContainer bulletContainer;
    //Private that are used everyframe
    Vector3 direction;
    float speed;


    //Temp variables used
    Vector3 newPosition;
    bool isSetup = false;


    public override void UpdateEventRaised(float deltaTime)
    {
        if (!isSetup)
        {
            isSetup = true;
            SetupBullet();
        }
        newPosition = transform.position + (direction * ( speed * deltaTime));
        //Debug.Log("Transform forward: " + transform.forward);
        //newPosition = transform.position + (transform.forward * (bulletContainer.cockingMechanism.bulletSpeed * deltaTime));
        transform.position = newPosition;
    }
    protected override void OnEnable()
    {
        base.OnEnable();
        isSetup = false;
        //SetupBullet();

    }
    void SetupBullet()
    {

        direction = transform.forward;
        direction.y = 0.0f;
        speed = bulletContainer.cockingMechanism.bulletSpeed;
    }
}
