﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireInputGetter : MonoBehaviour
{
    public GameEvent Fire1;
    public GameEvent Fire2;
    public GameEvent Reload;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Fire1.Raise();
        }
        if (Input.GetButtonDown("Fire2"))
        {
            Fire2.Raise();
        }
        if (Input.GetButtonDown("Reload"))
        {
            Reload.Raise();
        }
    }
}
