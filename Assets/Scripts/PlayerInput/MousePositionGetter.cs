﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousePositionGetter : OnUpdate
{
    public Vector3Variable positionOfAim;
    public Vector3Variable playerPosition;
    Vector2 prevPosition;
    Vector2 mousePosition;
    Camera mainCamera;

    Vector3 playerTransformInScreenSpace;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
        positionOfAim.SetValue(Vector2.zero);
        prevPosition = Vector2.zero;
    }

    // Update is called once per frame
    public override void UpdateEventRaised(float deltaTime)
    {
        mousePosition = Input.mousePosition;
        if (mousePosition != prevPosition)
        {
            playerTransformInScreenSpace = mainCamera.WorldToScreenPoint(playerPosition.Value);
            prevPosition = mousePosition;
            mousePosition.x -= playerTransformInScreenSpace.x;
            mousePosition.y -= playerTransformInScreenSpace.y;
            positionOfAim.SetValue( mousePosition.normalized);
        }
    }
}
