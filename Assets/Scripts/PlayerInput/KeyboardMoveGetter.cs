﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardMoveGetter : MonoBehaviour
{
    public Vector3Variable playerMovementInput;
    Vector3 input;
    // Update is called once per frame
    void Update()
    {
        input = Vector3.zero;
        input.x = Input.GetAxis("Horizontal");
        input.z = Input.GetAxis("Vertical");
        //Debug.Log("Ver: " + Input.GetAxis("Vertical") + " hori: " + Input.GetAxis("Horizontal"));
        playerMovementInput.SetValue(input.normalized);
    }
}
