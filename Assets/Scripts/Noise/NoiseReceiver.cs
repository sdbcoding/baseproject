﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseReceiver : MonoBehaviour {

	public virtual void ReceiveNoise(Vector3 location, float noiseIntensity)
	{

	}
	public virtual void ReceiveNoise(Vector3 location,float noiseIntensity,int noiseID)
	{
		
	}
}
