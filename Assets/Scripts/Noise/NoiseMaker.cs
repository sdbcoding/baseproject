﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMaker : MonoBehaviour {
	RectTransform noiseVisual;
	public static int noiseId;
	void Awake()
	{
		GameObject temp = Instantiate(Resources.Load("NoiseSystem/NoiseZone")) as GameObject;
		noiseVisual =  temp.GetComponent<RectTransform>();
		noiseVisual.gameObject.SetActive(false);
	}
	public static void IncreaseNoiseID()
	{
		noiseId++;
	}
	public void MakeNoise(Vector3 location, float noiseIntensity)
	{	
		noiseVisual.gameObject.SetActive(true);
		Vector3 newVisualPosition = location;
		newVisualPosition.y = 0.1f;
		noiseVisual.position = newVisualPosition;
		noiseVisual.sizeDelta = new Vector2(noiseIntensity*2,noiseIntensity*2);
		CancelInvoke("HideVisual");
		Invoke("HideVisual",0.1f);
		//Debug.Log("Making that sweet sweet noise");
		Collider[] collidersInSphere = Physics.OverlapSphere(location,noiseIntensity);
		if (collidersInSphere.Length == 0)
		{
			return;
		}
		for (int i = 0; i < collidersInSphere.Length; i++)
		{
			NoiseReceiver[] receivers = collidersInSphere[i].GetComponents<NoiseReceiver>();
			if (receivers.Length == 0)
			{
				continue;
			}
			for (int j = 0; j < receivers.Length; j++)
			{
				float distance = Vector3.Distance(location,collidersInSphere[i].transform.position);
				//receivers[j].ReceiveNoise(location,distance);
				receivers[j].ReceiveNoise(location,distance);
				//Debug.Log("noise ID: " + noiseId);
			}
		}
	}
	public void MakeNoise(Vector3 location, float noiseIntensity,int noiseID)
	{	
		noiseVisual.gameObject.SetActive(true);
		Vector3 newVisualPosition = location;
		newVisualPosition.y = 0.1f;
		noiseVisual.position = newVisualPosition;
		noiseVisual.sizeDelta = new Vector2(noiseIntensity*2,noiseIntensity*2);
		CancelInvoke("HideVisual");
		Invoke("HideVisual",0.1f);
		//Debug.Log("Making that sweet sweet noise");
		Collider[] collidersInSphere = Physics.OverlapSphere(location,noiseIntensity);
		if (collidersInSphere.Length == 0)
		{
			return;
		}
		for (int i = 0; i < collidersInSphere.Length; i++)
		{
			NoiseReceiver[] receivers = collidersInSphere[i].GetComponents<NoiseReceiver>();
			if (receivers.Length == 0)
			{
				continue;
			}
			for (int j = 0; j < receivers.Length; j++)
			{
				float distance = Vector3.Distance(location,collidersInSphere[i].transform.position);
				//receivers[j].ReceiveNoise(location,distance);
				receivers[j].ReceiveNoise(location,distance,noiseID);
				//Debug.Log("noise ID: " + noiseId);
			}
		}
	}
	void HideVisual()
	{
		noiseVisual.gameObject.SetActive(false);
	}
	public void MakeNoiseAtAttachedTransform(float noiseIntensity)
	{
		MakeNoise(transform.position,noiseIntensity);
	}
}
