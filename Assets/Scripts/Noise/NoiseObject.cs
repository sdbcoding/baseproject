﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseObject : NoiseMaker {
	[Range(0,30)]
	public float noiseRadius;
	[Range(0,5)]
	public float playerConnectionRadius;
	
	[SerializeField]
	ThingSet playerSet;
	Transform playerTransform;
	bool isRunning = false;
	// Use this for initialization
	void Start () {
		playerTransform = playerSet.GetItems()[0].transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (isRunning)
		{
			CheckDistanceToPlayer();
		}
	}
	void CheckDistanceToPlayer()
	{
		Vector3 yCorrectedPosition = transform.position;
		yCorrectedPosition.y = 0.5f;
		Vector3 yCorrectedPlayer = playerTransform.position;
		yCorrectedPlayer.y = 0.5f;

		float distance = Vector3.Distance(yCorrectedPlayer,yCorrectedPosition);
		if (distance < playerConnectionRadius)
		{
			MakeNoiseAtAttachedTransform(noiseRadius);
		}
	}
	public void StartChecking()
	{
		isRunning = true;
	}
	public void StopChecking()
	{
		isRunning = false;
	}
}
