﻿using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(Patrol))]

public class PatrolEditor : Editor {

	public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUI.enabled = !Application.isPlaying;

        Patrol e = target as Patrol;
        if (GUILayout.Button("SpawnWaypoint"))
            e.SpawnWayPoint();
		if (GUILayout.Button("DeleteLast"))
            e.DeleteLastWayPoint();
    }
}
