﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public FloatReference startingHealth;
    public float lastDamageTime;
    int currentHealth;
    public float HealthProcentage
    {
        get
        {
            return currentHealth / startingHealth;
        }
    }
    public UnityEvent deathEvent;
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = (int)startingHealth.Value;
    }
    public int GetCurrentHealth()
    {
        return currentHealth;
    }
    public void RemoveDamage(int amount)
    {
        currentHealth += amount;
        if (currentHealth > startingHealth)
        {
            currentHealth = (int)startingHealth.Value;
        }
    }

    public void TakeDamage(int amount)
    {
        lastDamageTime = GameTime.ElapsedTime;
        currentHealth -= amount;
        //Debug.Log(currentHealth);
        if (currentHealth <= 0)
        {
            Death();
        }
    }
    void Death()
    {
        deathEvent.Invoke();
    }
}
