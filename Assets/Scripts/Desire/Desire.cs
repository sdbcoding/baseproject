﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Desire", menuName = "DesireSystem/Desire", order = 0)]

public class Desire : ScriptableObject
{
    public bool UseInertia = true;
    public float beginningInertia = 0.1f;
    public float stepInertia = 0.002f;
    public float maxInertia = 0.2f;

    public CharacterAction[] actions;

    public Desire[] desires;

    public Consideration consideration;


    //public ResponseCurve responseCurve;


}
