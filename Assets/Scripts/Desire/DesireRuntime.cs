﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesireRuntime
{
    //Inertia
    bool UseInertia;
    public float[] inertia;
    float beginningInertia;
    float stepInertia;
    float maxInertia;

    public ConsiderationRuntime considerationRuntime;
    CharacterAction[] actions;
    public DesireRuntime[] desireRuntimes;

    public float[] desireValues;

    public string name;
    public bool isActionDesire;
    CharacterActionController actionController;

    int lastHighest = -1;


    public DesireRuntime(Desire desire,CharacterActionController characterActionController, Transform transform)
    {
        if (desire.consideration == null)
        {
            Debug.LogError("No consideration added to desire, desire will never be selected");
            return;
        }
        considerationRuntime = new ConsiderationRuntime(desire.consideration, transform);
        name = desire.name;
        
        if (desire.desires.Length != 0)
        {
            desireRuntimes = new DesireRuntime[desire.desires.Length];
            desireValues = new float[desire.desires.Length];
            for (int i = 0; i < desire.desires.Length; i++)
            {
                desireRuntimes[i] = new DesireRuntime(desire.desires[i],characterActionController, transform);
            }
            isActionDesire = false;

            //Inertia
            UseInertia = desire.UseInertia;
            inertia = new float[desire.desires.Length];
            beginningInertia = desire.beginningInertia;
            stepInertia = desire.stepInertia;
            maxInertia = desire.maxInertia;
        }
        else
        {
            isActionDesire = true;
            actions = desire.actions;
            actionController = characterActionController;
        }
    }
    public float GetValue()
    {
        return considerationRuntime.GetValue();
    }

    public void RunActions()
    {
        if (isActionDesire)
        {
            actionController.StartNewActions(actions);
            Debug.Log("Running : " + name);
        } else
        {
            FindHeighestDesire();
        }
    }

    void FindHeighestDesire()
    {
        int currentHighest = 0;
        desireValues[0] = desireRuntimes[0].GetValue();
        float highestValue = desireValues[0] + inertia[0];
        for (int i = 1; i < desireRuntimes.Length; i++)
        {
            desireValues[i] = desireRuntimes[i].GetValue();
            if ((desireValues[i] + inertia[i]) > highestValue)
            {
                highestValue = desireValues[i];
                currentHighest = i;
            }
        }

        if(currentHighest != lastHighest)
        {
            lastHighest = currentHighest;
            desireRuntimes[currentHighest].RunActions();
            if (UseInertia)
            {
                inertia = new float[inertia.Length];
                inertia[currentHighest] = beginningInertia;
            }
        } else
        {
            if (UseInertia)
            {
                inertia[currentHighest] = stepInertia;
                if (inertia[currentHighest] > maxInertia)
                {
                    inertia[currentHighest] = maxInertia;
                }
            }

        }
    }
    public void PrintDesireValues()
    {
        string whatToPrint = "";
        for (int i = 0; i < desireValues.Length; i++)
        {
            whatToPrint += desireRuntimes[i].name + ": " + desireValues[i] + "\n";
        }
        Debug.Log(whatToPrint);
    }
    public float[] GetValues()
    {
        return desireValues;
    }
}
