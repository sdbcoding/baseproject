﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesireController : MonoBehaviour
{
#if UNITY_EDITOR
    [Multiline(12)]
    public string desireValueString = "";
#endif
    public CharacterActionController characterActionController;
    public Desire desire;
    [Range(0.0f,1.5f)]
    public float howOftenToRunCheck = 0.1f;

    float currentValue;
    DesireRuntime desireRuntime;

    private void Start()
    {
        if (desire == null)
        {
            Debug.LogError("No root desire added to character: " + gameObject.name + "aborting");
            Destroy(this);
            return;
        }
        desireRuntime = new DesireRuntime(desire, characterActionController, transform);
        
    }

    public void RunDesires(float deltaTime)
    {
        currentValue += deltaTime;
        if (howOftenToRunCheck < currentValue)
        {
            desireRuntime.RunActions();

#if UNITY_EDITOR
            //desireRuntime.PrintDesireValues();
            UpdateEditorString();
#endif
            currentValue = 0;
        }
    }
#if UNITY_EDITOR
    void UpdateEditorString()
    {
        desireValueString = "";
        AddDesireToString(desireRuntime);
    }

    void AddDesireToString(DesireRuntime desireRuntimeForPrint)
    {
        float[] values = desireRuntimeForPrint.GetValues();
        DesireRuntime[] runtimes = desireRuntimeForPrint.desireRuntimes;
        for (int i = 0; i < values.Length; i++)
        {
            desireValueString += runtimes[i].name + " " + values[i].ToString() + "\n";
            if (!runtimes[i].isActionDesire)
            {
                AddDesireToString(runtimes[i]);
            }
            else
            {
                AddActionDesireToString(runtimes[i]);
            }
        }
    }
    void AddActionDesireToString(DesireRuntime desireRuntimeForPrint)
    {
        //desireValueString += " - " + desireRuntime.considerationRuntime.name + ": " + desireRuntime.considerationRuntime.values[0] + " - " + desireRuntime.considerationRuntime.values[1] + "\n";
        for (int i = 0; i < 2; i++)
        {
            if (desireRuntimeForPrint.considerationRuntime.UseConsideration[i])
            {
                AddConsiderationToString(desireRuntimeForPrint.considerationRuntime.considerationRuntime[i]);
            }
        }
    }
    void AddConsiderationToString(ConsiderationRuntime considerationRuntime)
    {
        desireValueString += " -- " + considerationRuntime.name + ": " + considerationRuntime.values[0] + " - " + considerationRuntime.values[1] + "\n";
        for (int i = 0; i < 2; i++)
        {
            if (considerationRuntime.UseConsideration[i])
            {
                AddConsiderationToString(considerationRuntime.considerationRuntime[i]);
            }
        }
    }
#endif

}
