﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ItemNamespace {

    public class Item : ScriptableObject
    {
        public string itemName;
        public string desciption;
        public ItemClass classType;


        public float timeToUse;
        public virtual void UseItem(GameObject characterUsingIt)
        {

        }
       
        public virtual void MakeActive(GameObject characterUsingIt)
        {

        }

    }
}
