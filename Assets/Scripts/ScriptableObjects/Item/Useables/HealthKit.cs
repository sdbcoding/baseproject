﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ItemNamespace;

[CreateAssetMenu(fileName = "HealthKit", menuName = "Item/Useable/HealthKit", order = 0)]

public class HealthKit : Item
{
    public int amountToHeal;

    public override void UseItem(GameObject characterUsingIt)
    {
        base.UseItem(characterUsingIt);
        Health health = characterUsingIt.GetComponent<NerveSystem>().health;
        health.RemoveDamage(amountToHeal);
    }


}
