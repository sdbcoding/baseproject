﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ItemNamespace
{
    [CreateAssetMenu(fileName = "Bullet", menuName = "Item/Bullet", order = 1)]

    public class Bullet : Item
    {
        public int damageAmount;
    }
}