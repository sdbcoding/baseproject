﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ItemNamespace{

    [CreateAssetMenu(fileName = "ClassType", menuName = "Item/ClassType", order = 0)]

    public class ItemClass : ScriptableObject
    {
        public string classType;
    }
}
