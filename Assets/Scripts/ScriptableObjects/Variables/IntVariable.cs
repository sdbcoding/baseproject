﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Variables", menuName = "Variables/Int Variable", order = 8)]
public class IntVariable : GameEvent
{
    [SerializeField]
    private int Value;

    public void SetValue(int value)
    {
        Value = value;
        Raise();
    }

    public void SetValue(IntVariable value)
    {
        Value = value.Value;
        Raise();

    }

    public int GetValue()
    {
        return Value;
    }

    public void ApplyChange(int amount)
    {
        Value += amount;
        Raise();
    }

    public void ApplyChange(IntVariable amount)
    {
        Value += amount.Value;
        Raise();
    }
}
