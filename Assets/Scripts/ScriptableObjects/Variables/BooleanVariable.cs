﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[CreateAssetMenu(fileName ="Variables", menuName = "Variables/Bool Variable", order = 0)]
public class BooleanVariable : GameEvent 
{
    public bool Value;

    public void SetValue(bool value)
    {
        Value = value;
        Raise();

    }

    public void SetValue(BooleanVariable value)
    {
        Value = value.Value;
        Raise();

    }

    public void ApplyChange(bool amount)
    {
        Value = amount;
        Raise();
    }

    public void ApplyChange(BooleanVariable amount)
    {
        Value = amount.Value;
        Raise();
    }
}
