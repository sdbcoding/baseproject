﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace WeaponNamespace{

    [CreateAssetMenu(fileName = "CockingMechanism", menuName = "Item/Weapons/CockingMechanism", order = 4)]
    public class CockingMechanism : WeaponAttachment
    {
        public enum FireringRate { Manual,Semi,Burst,Auto};
        public FireringRate fireringRate;
        public float bulletSpeed;
        public float recoil;
    }
}
