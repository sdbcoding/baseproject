﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponNamespace
{

    [CreateAssetMenu(fileName = "Magazine", menuName = "Item/Weapons/Magazine", order = 3)]
    public class Magazine : WeaponAttachment
    {
        public int capacity;
        public float reloadTime;
    }
}
