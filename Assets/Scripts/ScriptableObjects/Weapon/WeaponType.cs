﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponNamespace
{
    [CreateAssetMenu(fileName = "WeaponType", menuName = "Item/Weapons/WeaponType", order = 8)]
    public class WeaponType : ScriptableObject
    {
        public string weaponType;
        public float relevantRange;
    }
}
