﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WeaponNamespace;
namespace ItemNamespace{
    [CreateAssetMenu(fileName = "Weapon", menuName = "Item/Weapons/BaseWeapon", order = 0)]

    public class Weapon : Item
    {
        public WeaponType weaponType;
        public CockingMechanism cockingMechanism;
        public Muzzle muzzle;
        public Magazine magazine;
        public Sight sight;

        public override void UseItem(GameObject characterUsingIt)
        {
            //Fire the weapon however the different attachments require. The enemy or player have already taken care of the legality of the shooting
            SpawnBullet spawnBullet = characterUsingIt.GetComponent<SpawnBullet>();
            if(spawnBullet == null)
            {
                Debug.Log("No SpawnBullet attached to gameobject: " +characterUsingIt.name);
                return;
            }

            //Find the spread and the starting angle
            float startingAngle;
            if (muzzle.spread == 0)
            {
                startingAngle = 0;
            } else
            {
                startingAngle = -(muzzle.spread / 2);

            }
            float angle = muzzle.spread / muzzle.numberOfBulletsSpawned;

            for (int i = 0; i < muzzle.numberOfBulletsSpawned; i++)
            {
                spawnBullet.SpawnBulletCommand(startingAngle + (angle * i));
            }
        }

        

        /*
        Magazine
            - Bullet amount
            - Reload time
        Muzzle
            - Bullets fired at once
            - Spread 
        Bullet
            - Damage
            - Special Effect
        Cocking Mechanism
            - Manual
            - Semi
            - Auto
            - Speed of bullet
        Scope
            - Accuracy
        Zoom in potential

         */

    }
}
