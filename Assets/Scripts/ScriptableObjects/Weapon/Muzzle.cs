﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace WeaponNamespace{
    [CreateAssetMenu(fileName = "Muzzle", menuName = "Item/Weapons/Muzzle", order = 2)]

    public class Muzzle : WeaponAttachment
    {
        public int numberOfBulletsSpawned;
        public int spread;
    }
}
