﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;

using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Consideration))]

public class ConsiderationGetter : Editor
{

    //GameObject[] prevGameObject = new GameObject[2];
    //string[][] optionsComponent = new string[2][];
    //int[] indexComponent = new int[2];

    //Component[] prevComponent = new Component[2];
    //string[][] optionsFloats = new string[2][];
    //int[] indexFloats = new int[2];

    GameObject[] prevGameObject = new GameObject[2];
    Component[] prevComponent = new Component[2];


    object[] reference = new object[2];

    
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        Consideration con = target as Consideration;

        if (con.obj.Length != 2)
        {
            //Editor stuff
            con.consideration = new Consideration[2];
            con.obj = new GameObject[2];
            con.listComp = new Component[2][];
            con.comp = new Component[2];
            con.floats = new List<object>[2];
            //con.prevGameObject = new GameObject[2];
            con.optionsComponent = new string[2][];
            con.indexComponent = new int[2];

            //con.prevComponent = new Component[2];
            con.optionsFloats = new string[2][];

        }


        string currentFloatString = "Nothing";
        string[] testString;
        //TODO: place the two boxes for gameobjects correctly
        if (!con.UseConsideration1 && con.floatName.Length == 2 && con.floatName[0] != null)
        {
            testString = con.floatName[0].Split(' ');
            if (testString.Length == 2)
            {
                currentFloatString = testString[1];
            } else
            {
                currentFloatString = "Nothing selected";
            }
            EditorGUILayout.LabelField("Current float" + 1 + ": ", currentFloatString);

        }
        if (!con.UseConsideration2 && con.floatName.Length == 2 && con.floatName[1] != null)
        {
            testString = con.floatName[1].Split(' ');
            if (testString.Length == 2)
            {
                currentFloatString = testString[1];
            }
            else
            {
                currentFloatString = "Nothing selected";
            }
            EditorGUILayout.LabelField("Current float" + 2 + ": ", currentFloatString);

        }
        SerializedProperty considerations = new SerializedObject(con).FindProperty("consideration");
        //Debug.Log(considerations.arraySize);
        if (!con.UseConsideration1)
        {

            con.obj[0] = (GameObject)EditorGUILayout.ObjectField(con.obj[0], typeof(GameObject), false);
            FindReference(con,0);
        } else
        {
            //EditorGUILayout.PropertyField(considerations.GetArrayElementAtIndex(0), new GUIContent("Consideration 1:") );
            con.consideration[0] = (Consideration)EditorGUILayout.ObjectField(con.consideration[0],typeof(Consideration),false);


        }
        if (!con.UseConsideration2)
        {
            
            con.obj[1] = (GameObject)EditorGUILayout.ObjectField(con.obj[1], typeof(GameObject), false);
            FindReference(con,1);
        } else
        {
            //EditorGUILayout.PropertyField(considerations.GetArrayElementAtIndex(1), new GUIContent("Consideration 2:"));
            con.consideration[1] = (Consideration)EditorGUILayout.ObjectField(con.consideration[1], typeof(Consideration), false);

        }



    }
    void ClearForIndex(Consideration con, int index)
    {
        
        reference[index] = null;
    }

    void FindReference(Consideration con, int index)
    {
        string labelText = "add gameobject";
        if (con.obj[index] != null)
        {
            labelText = "Select the desired component";

            if (prevGameObject[index] != con.obj[index])
            {
                con.comp[index] = null;
                ClearForIndex(con,index);
                con.listComp[index] = con.obj[index].GetComponentsInChildren(typeof(Component));
                //Debug.Log(con.listComp[index].Length);
                con.optionsComponent[index] = new string[con.listComp[index].Length];
                for (int i = 0; i < con.optionsComponent[index].Length; i++)
                {
                    con.optionsComponent[index][i] = con.listComp[index][i].ToString();
                }
            }

            con.indexComponent[index] = EditorGUILayout.Popup(con.indexComponent[index], con.optionsComponent[index]);
            con.comp[index] = con.listComp[index][con.indexComponent[index]];
            //Debug.Log("Component: " + con.comp.GetType());
            prevGameObject[index] = con.obj[index];
        } else
        {
            con.comp[index] = null;
            con.floats[index] = null;
            ClearForIndex(con, index); 
        }
        if (con.comp[index] != null)
        {
            labelText = "Select the desired float variable";

            con.floats[index] = new List<object>();
            FieldInfo[] fields = con.comp[index].GetType().GetFields();
            foreach (var thisVar in fields)
            {
                //Debug.Log(typeof(float) + " var: " + thisVar.FieldType);
                //Debug.Log(thisVar.GetValue(thisV))
                //if (thisVar.FieldType == typeof(float))
                //{
                //    Debug.Log("is true");
                //}
                try
                {
                    if (thisVar.FieldType == typeof(float))
                    {
                        con.floats[index].Add(thisVar);
                    } else if (thisVar.FieldType == typeof(bool))
                    {
                        //Debug.Log("We got a bool");
                        con.floats[index].Add(thisVar);
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("We're down here");
                    Debug.LogError(e);
                }
            }

            PropertyInfo[] properties = con.comp[index].GetType().GetProperties();
            foreach (var thisVar in properties)
            {
                //Debug.Log(typeof(float) + " var: " + thisVar.FieldType);
                //Debug.Log(thisVar.GetValue(thisV))
                //if (thisVar.FieldType == typeof(float))
                //{
                //    Debug.Log("is true");
                //}
                try
                {
                    if (thisVar.PropertyType == typeof(float))
                    {
                        con.floats[index].Add(thisVar);
                    }
                    else if (thisVar.PropertyType == typeof(bool))
                    {
                        //Debug.Log("We got a bool");
                        con.floats[index].Add(thisVar);
                    }
                    //Debug.Log(thisVar.PropertyType.Name);
                }
                catch (Exception e)
                {
                    Debug.Log("We're down here");
                    Debug.LogError(e);
                }
            }

        }
        else
        {
            con.floats[index] = null;
            ClearForIndex(con, index);
        }

        if (con.floats[index] != null && con.floats[index].Count != 0)
        {
            if (con.floats[index].Count != 0)
            {
                if (prevComponent[index] != con.comp[index])
                {
                    con.optionsFloats[index] = new string[con.floats[index].Count];
                    for (int i = 0; i < con.optionsFloats[index].Length; i++)
                    {
                        //TODO: Clean up the avalible stuff so System.Single is not there
                        //string[] test = con.floats[i].ToString().Split(null);
                        //string test.Split(null);
                        //Debug.Log("TEst:" + test.Length);
                        con.optionsFloats[index][i] = con.floats[index][i].ToString();
                    }
                    //Debug.Log("running");

                } else
                {
                    ClearForIndex(con, index);
                }
                con.indexFloats[index] = EditorGUILayout.Popup(con.indexFloats[index], con.optionsFloats[index]);
                reference[index] = con.floats[index][con.indexFloats[index]];
                prevComponent[index] = con.comp[index];
            }
        } else
        {
            ClearForIndex(con, index);
        }

        if (reference[index] != null)
        {
            labelText = "Press Save to save the changes";

            GUI.enabled = !Application.isPlaying;

            if (GUILayout.Button("Save"))
            {
                if (con.componentName.Length != 2)
                {
                    Debug.Log("Its null");
                    con.componentName = new string[2];
                    con.gameObjectName = new string[2];
                    con.floatName = new string[2];
                }
                Debug.Log("Comp: " + con.componentName.Length);
                con.componentName[index] = con.comp[index].GetType().ToString();
                con.gameObjectName[index] = con.obj[index].name;
                con.floatName[index] = reference[index].ToString();
                Debug.Log(con.floatName[0] + con.floatName[1]);

            }
        }

        EditorGUILayout.LabelField("Consideration "+ (index + 1) +": ", labelText);
    }
    
}
