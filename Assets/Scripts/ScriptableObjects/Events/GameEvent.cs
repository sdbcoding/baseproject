﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "Events/GameEvent", order = 0)]

public class GameEvent : ScriptableObject {
     #if UNITY_EDITOR
    [Multiline]
    public string DeveloperDescription = "";
    #endif
    protected List<GameEventListener> listeners = new List<GameEventListener>();

    public void Awake()
    {
        listeners = new List<GameEventListener>();
    }
    public virtual void Raise()
    {
        CheckListenersForNull();

        for (int i = listeners.Count -1; i >= 0; i--)
        {
            listeners[i].OnEventRaised();
        }
    }

    public void RegisterListener(GameEventListener listener)
    {
        if (!listeners.Contains(listener))
            listeners.Add(listener);
    }
    public void UnRegisterListener(GameEventListener listener)
    {
        if (listeners.Contains(listener))
            listeners.Remove(listener);
    }
    public int GetListenerListSize()
    {
        CheckListenersForNull();
        return listeners.Count;
    }
    void CheckListenersForNull()
    {
        for (int i = listeners.Count - 1; i >=0 ; i--)
        {
            if (listeners[i] == null)
            {
                listeners.RemoveAt(i);
            }
        }
    }
}
