﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="Reaction", menuName = "Reaction/RunStraight", order = 4)]

public class ReactionMinedlessRunStraight : Reaction {
	public LayerMask mask;

	public float distanceToCheck = 10.0f;
	public float stunTime = 2;
	public override bool DoReaction(ReactionController controller, int index)
	{
		base.DoReaction( controller, index);
		switch (index)
		{
			case 0:
			SetDestination(FindClosestCollider(controller.transform.position,controller.transform.forward),controller);
			break;
			case 1:
			SetDestination(FindClosestCollider(controller.transform.position,FindDirection(controller.transform.position, controller.transform.forward)),controller);
			break;
			case 2:
			controller.enemyMovement.SetDestination(controller.transform.position, controller.FinishedReaction,stunTime);
			break;
		}
		return true;
	}
	Vector3 FindDirection(Vector3 position, Vector3 direction)
	{
		RaycastHit hit;
		Physics.Raycast(position,direction,out hit,distanceToCheck,mask);
		if (hit.collider == null)
		{
			return direction;
		}
		Vector3 newDirection = Vector3.Reflect(direction,hit.normal);
		return newDirection;
	}
	Vector3 FindClosestCollider(Vector3 position, Vector3 direction)
	{
		RaycastHit hit;
		Physics.Raycast(position,direction,out hit,distanceToCheck,mask);
		if (hit.collider == null)
		{
			return position + (direction * distanceToCheck);
		}
		return hit.point;
	}
	void SetDestination(Vector3 destination, ReactionController controller)
	{
		
		controller.enemyMovement.SetDestination(destination,controller.FinishedReaction);
	}
	void FindNextSpo()
	{

	}
	public override int UpdateIndex(int lastIndex)
	{
		
		if (lastIndex == 1)
		{
			return 2;
		}
		return 1;
	}
	
}
