﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reaction : ScriptableObject {

	public float time;
	public bool isOnTimer;
	public bool isTerrified;

	public virtual bool DoReaction(ReactionController controller,int index)
	{
		if (!controller.GetIsTimerRunning() && isOnTimer)
		{
			controller.StartTimer(time,controller.ClearCurrentReaction);
			return true;
		}
		return false;
	}
	public virtual int UpdateIndex(int lastIndex)

	{
		return 0;
	}
	
}
