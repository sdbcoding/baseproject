﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="Reaction", menuName = "Reaction/GoToThingSetTIMED", order = 2)]

public class ReactionGoToThingSetTimed : Reaction {

	public enum SpotChosenSystem {Closest,FurthestAway,Random}
	public ThingSet safeSpots;
	public SpotChosenSystem chosenReason;
	public bool oneTime;

	public override bool DoReaction(ReactionController controller, int index)
	{
		base.DoReaction( controller, index);
		switch (chosenReason)
		{
			case SpotChosenSystem.Closest:
				SetDestination(ReturnNearestSafeSpot(controller),controller);
			return oneTime;
			case SpotChosenSystem.FurthestAway:
				SetDestination(ReturnFurthesSafeSpot(controller),controller);

			return oneTime;
			case SpotChosenSystem.Random:
			SetDestination(ReturnRandom(),controller);
			return oneTime;
		}
		return false;
	}
	public override int UpdateIndex(int lastIndex)
	{
		return 0;
	}

	void SetDestination(Vector3 destination, ReactionController controller)
	{
		
		controller.enemyMovement.SetDestination(destination,controller.FinishedReaction);
	}
	Vector3 ReturnRandom()
	{
		return safeSpots.GetItems()[UnityEngine.Random.Range(0,safeSpots.GetItems().Count)].transform.position;
	}
	Vector3 ReturnNearestSafeSpot(ReactionController controller)
	{
		Vector3 closestSpot = safeSpots.GetItems()[0].transform.position;
		closestSpot.y = controller.transform.position.y;
		float closestDistance = Vector3.Distance(closestSpot,controller.transform.position);
		for (int i = 1; i < safeSpots.GetItems().Count; i++)
		{
			Vector3 newTest = safeSpots.GetItems()[i].transform.position;
			newTest.y = controller.transform.position.y;
			float distance = Vector3.Distance(newTest,controller.transform.position);
			if (distance < closestDistance)
			{
				closestDistance = distance;
				closestSpot = newTest;
			}
		}
		return closestSpot;
	}
	Vector3 ReturnFurthesSafeSpot(ReactionController controller)
	{
		Vector3 furthest = safeSpots.GetItems()[0].transform.position;
		furthest.y = controller.transform.position.y;
		float futhestDistance = Vector3.Distance(furthest,controller.transform.position);
		for (int i = 1; i < safeSpots.GetItems().Count; i++)
		{
			Vector3 newTest = safeSpots.GetItems()[i].transform.position;
			newTest.y = controller.transform.position.y;
			float distance = Vector3.Distance(newTest,controller.transform.position);
			if (distance > futhestDistance)
			{
				futhestDistance = distance;
				furthest = newTest;
			}
		}
		return furthest;
	}
}
