﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="Reaction", menuName = "Reaction/RunIncircles", order = 3)]

public class ReactionRunInCircles : Reaction {
	public override bool DoReaction(ReactionController controller, int index)
	{
		base.DoReaction( controller, index);
		
		Vector3 leftForward = (controller.transform.forward + -controller.transform.right).normalized;
		Vector3 newLocation = controller.transform.position+ leftForward;
		controller.enemyMovement.SetDestination(newLocation,controller.FinishedReaction);
		return true;
	}

	public override int UpdateIndex(int lastIndex)
	{
		return 0;
	}
}
