﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="Reaction", menuName = "Reaction/FollowPatrol", order = 5)]

public class ReactionPatrol : Reaction {
	public override bool DoReaction(ReactionController controller, int index)
	{
		base.DoReaction( controller, index);

		//Debug.Log("Incoming index: " + index);
		Vector3 nextPosition;
		if (index == 0)
		{
			nextPosition = FindClosestPatrolPoint(controller);
		} else {
			nextPosition = FindNextWaypoint(controller,index);
		}
		SetDestination(nextPosition,controller);
		return true;
	}
	void SetDestination(Vector3 destination, ReactionController controller)
	{
		
		controller.enemyMovement.SetDestination(destination,controller.FinishedReaction);
	}

	Vector3 FindNextWaypoint(ReactionController controller, int index)
	{
		//Debug.Log("Count size: " + controller.connectedPatrol.GetPatrol().Items.Count);
		//Debug.Log("Index when finding new waypoint:" + index);
		if (index == controller.connectedPatrol.GetPatrol().GetItems().Count + 1)
		{
			//Debug.Log("Resetting");
			controller.SetIndex(0);
			return controller.connectedPatrol.GetPatrol().GetItems()[0];
		}
		return controller.connectedPatrol.GetPatrol().GetItems()[index-1];
	}
	Vector3 FindClosestPatrolPoint(ReactionController controller)
	{
		float closestDistance = Vector3.Distance(controller.connectedPatrol.GetPatrol().GetItems()[0],controller.transform.position);
        int closestWaypoint = 0;
        for (int i = 1; i < controller.connectedPatrol.GetPatrol().GetItems().Count; i++)
        {
            float distance = Vector3.Distance(controller.connectedPatrol.GetPatrol().GetItems()[i],controller.transform.position);
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestWaypoint = i;
            }
        }
		controller.SetIndex(closestWaypoint +1);
		//Debug.Log("new Index after closest: " + (closestWaypoint +1));
		return controller.connectedPatrol.GetPatrol().GetItems()[closestWaypoint];
	}


	public override int UpdateIndex(int lastIndex)
	{
		//Debug.Log(lastIndex+1);
		lastIndex++;
		return lastIndex;
	}

}
