﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Color Set", menuName = "Sets/Color Set", order = 1)]

public class ColourSet : RunTimeSets<Color> {
    
    public Color startColor;
    public bool Reset;
    
    void OnEnable()
    {
        if(Reset)
        {
            for (int i = 0; i < Items.Count; i++)
            {
                Items[i] = startColor;
            }
        }
    }
    
	
}
