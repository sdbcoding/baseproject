﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SceneSet", menuName = "PowerTools/SceneSet")]
public class SceneSet : ScriptableObject
{
    // Use these to control what happens when you hit play in editor with
    // a particular sceneset open
    public enum SceneSetType
    {
        Generic,
        Gameplay,
        Menu
    }

#if UNITY_EDITOR
    public UnityEditor.SceneAsset[] scenes;

    void OnValidate()
    {
        scenePaths = new string[scenes.Length];
        for (int i = 0; i < scenes.Length; i++)
        {
            scenePaths[i] = UnityEditor.AssetDatabase.GetAssetPath(scenes[i]);
        }
    }
#endif
    public string[] scenePaths;



    [Tooltip("The leveltype determines e.g. what happens when you hit play in editor")]
    public SceneSetType sceneSetType = SceneSetType.Gameplay;
}
