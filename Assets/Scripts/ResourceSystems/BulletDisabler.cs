﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDisabler : OnUpdate
{
    public ThingSet bullets;
    public ThingSet playerSet;
    public FloatReference acceptableDistance;
    float distance;

    public override void UpdateEventRaised(float deltaTime)
    {
        for (int i = bullets.GetItemListSize() -1; i >= 0; i--)
        {
            distance = Vector3.Distance(playerSet.GetItemsAtIndex(0).transform.position, bullets.GetItemsAtIndex(i).transform.position);
            if (distance > acceptableDistance.Value)
            {
                bullets.GetItemsAtIndex(i).gameObject.SetActive(false);
            }
        }
    }

}
