﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateLoopController : MonoBehaviour {
	public UpdateEvent updateLoop;
	bool isRunning = true;
	public FloatReference timeSpeed;

	void Start()
	{
		Debug.LogWarning("TimeSpeed is set to zero, updateloop won't run unless changed");
	}	
	void Update () {
		if (isRunning)
		{
			updateLoop.Raise(Time.deltaTime * timeSpeed);
		}
	}
	public void StopUpdateLoop()
	{
		isRunning = false;
		//Debug.Log("UpdateLoop is stopped");
	}
	public void RunUpdateLoop()
	{
		isRunning = true;
		//Debug.Log("UpdateLoop is running");
	}
}
