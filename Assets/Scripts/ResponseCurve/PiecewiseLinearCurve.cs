﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "PiecewiseLinearCurve", menuName = "DesireSystem/ResponseCurve/PiecewiseLinearCurve", order = 2)]

public class PiecewiseLinearCurve : ResponseCurve
{
    public int[] cutOffPoints;
    float[] usedFloat;

    private void OnEnable()
    {
        if (cutOffPoints == null || cutOffPoints.Length == 0)
        {
            Debug.LogError("No cutoffpoints selected in response curve!");
        }
        SetupFloatArray();

    }
    void DebugFloat()
    {
        for (int i = 0; i < usedFloat.Length; i++)
        {
            Debug.Log(usedFloat[i]);
        }
    }
    void SetupFloatArray()
    {
        usedFloat = new float[cutOffPoints.Length];
        for (int i = 0; i < usedFloat.Length; i++)
        {
            usedFloat[i] = (float)cutOffPoints[i] / 100.0f;
        }
        System.Array.Sort(usedFloat);
        DebugFloat();
    }

    public override float Response(float value1, float value2)
    {
#if UNITY_EDITOR
        SetupFloatArray();
#endif
        float tempValue = value1 * value2;
        if (tempValue > usedFloat[usedFloat.Length -1])
        {
            return FindValueBetweenTwo(usedFloat[usedFloat.Length - 1], 1.0f,tempValue);
        }
        for (int i = usedFloat.Length - 1; i >= 0; i--)
        {
            if (tempValue > usedFloat[i])
            {
                return FindValueBetweenTwo(usedFloat[i], usedFloat[i + 1], tempValue);
            }
        }
        return FindValueBetweenTwo(0.0f, usedFloat[0], tempValue);

    }

    float FindValueBetweenTwo(float value1, float value2, float realValue)
    {
        return (realValue - value1) / (value2 - value1);
    }

}
