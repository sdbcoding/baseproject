﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "InverseResponseCurve", menuName = "DesireSystem/ResponseCurve/InverseResponseCurve", order = 0)]

public class BothInverse : ResponseCurve
{
    public bool inverseA = false;
    public bool inverseB = false;

    public override float Response(float value1, float value2)
    {
        //Debug.Log("v1: " + value1 + " v2: " + value2);
        float[] newValues = new float[2];
        if (inverseA)
        {
            newValues[0] = 1.0f - value1;
        } else
        {
            newValues[0] = value1;

        }

        if (inverseB)
        {
            newValues[1] = 1.0f - value2;
        }
        else
        {
            newValues[1] = value2;

        }
        //Debug.Log("v1: " + newValues[0] + " v2: " + newValues[1]);

        return base.Response(newValues[0], newValues[1]);
    }
}
