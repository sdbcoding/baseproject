﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "QuadraticResponseCurve", menuName = "DesireSystem/ResponseCurve/QuadraticResponseCurve", order = 1)]

public class QuadraticResponseCurve : ResponseCurve
{
    public float exponent;
    public bool rotated = false;
    public override float Response(float value1, float value2)
    {
        float tempValue = value1 * value2;
        if (tempValue > 1.0f)
        {
            tempValue = 1.0f;
        }
        if (rotated)
        {
            return 1 - Mathf.Pow(tempValue / 1.0f, exponent);

        }
        return Mathf.Pow(tempValue / 1.0f, exponent);
    }

}
