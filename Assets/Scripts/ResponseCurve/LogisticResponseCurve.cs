﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "LogisticResponseCurve", menuName = "DesireSystem/ResponseCurve/LogisticResponseCurve", order = 2)]

public class LogisticResponseCurve : ResponseCurve
{
    public float exponent;
    public override float Response(float value1, float value2)
    {
        float tempValue = value1 * value2;
        if (tempValue > 1.0f)
        {
            tempValue = 1.0f;
        }
        
        return (value1 * value2) * 1.0f / (1.0f + Mathf.Exp(exponent));
    }
}
