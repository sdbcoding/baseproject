﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResponseCurve : ScriptableObject
{
    
    public virtual float Response(float value1, float value2)
    {
        return value1 * value2;
    }
}
