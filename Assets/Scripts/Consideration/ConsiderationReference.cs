﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ConsiderationReference
{
    public bool UseConstant = false;
    public float ConstantValue;
    public Consideration Variable;

    public ConsiderationReference()
    { }
    public ConsiderationReference(float value1)
    {
        UseConstant = true;
        ConstantValue = value1;

    }

    /*
    public float GetConsiderationCalculation()
    {
        if (responseCurve == null)
        {
            return Mathf.Clamp01((UseConstant ? ConstantValue : Variable1.Value) + (UseConstant2 ? ConstantValue2 : Variable2.Value));
        }

        return responseCurve.Response((UseConstant1 ? ConstantValue1 : Variable1.Value), (UseConstant2 ? ConstantValue2 : Variable2.Value));
    }
    */

   



    public ResponseCurve responseCurve;
}
