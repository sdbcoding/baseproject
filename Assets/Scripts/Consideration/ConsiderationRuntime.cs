﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;


public class ConsiderationRuntime 
{
    public string name;
    public bool[] UseConsideration;
    string[] componentNames;
    string[] varibleNames;

    FieldInfo[] fieldInfos;
    PropertyInfo[] properties;

    Component[] connectedComponent;
    string[] floatString;
    List<object>[] floats;
    int[] variblePosition;
    public float[] values = new float[2];


    public ConsiderationRuntime[] considerationRuntime;

    ResponseCurve responseCurve;

    public ConsiderationRuntime(Consideration consideration, Transform transform)
    {
        name = consideration.name;
        UseConsideration = new bool[] { consideration.UseConsideration1, consideration.UseConsideration2 };
        variblePosition = new int[2];
        connectedComponent = new Component[2];
        considerationRuntime = new ConsiderationRuntime[2];

        fieldInfos = new FieldInfo[2];
        properties = new PropertyInfo[2];

        for (int i = 0; i < UseConsideration.Length; i++)
        {
            if (UseConsideration[i])
            {
                considerationRuntime[i] = new ConsiderationRuntime(consideration.consideration[i], transform);
            } else
            {
                FindReference(consideration,transform,i);
            }
        }
        if (consideration.responseCurve == null)
        {
            Debug.LogWarning("No response curve selected");
            responseCurve = ScriptableObject.CreateInstance<ResponseCurve>();
        } else
        {
            responseCurve = consideration.responseCurve;
        }
    }

    public float GetValue()
    {
        for (int i = 0; i < values.Length; i++)
        {
            if (UseConsideration[i])
            {
                values[i] = considerationRuntime[i].GetValue();
            } else
            {
                //values[i] = (float)floats[i][variblePosition[i]];
                //values[i] = connectedGameObjects[i].GetComponent(Type.GetType(componentNames[i])).GetType().GetField(varibleNames[i]);
                //Debug.Log(name);
                //Debug.Log(connectedComponent[i].GetType());
                //Debug.Log(fieldInfos[i].GetValue(connectedComponent[i]));
                //Debug.Log(fieldInfos[i].FieldType);
                if (fieldInfos[i] != null)
                {
                    if (fieldInfos[i].FieldType == typeof(bool))
                    {
                        values[i] = (bool)fieldInfos[i].GetValue(connectedComponent[i]) ? 1 : 0;
                    } else
                    {
                        values[i] = (float)fieldInfos[i].GetValue(connectedComponent[i]);   

                    }
                } else
                {
                    //Debug.Log(properties[i].Name);
                    //Debug.Log(connectedComponent[i].GetType());
                    if (properties[i].PropertyType == typeof(bool))
                    {
                        values[i] = (bool)properties[i].GetValue(connectedComponent[i]) ? 1 : 0;
                    } else
                    {
                        values[i] = (float)properties[i].GetValue(connectedComponent[i]);
                    }
                }
            }
        }
        for (int i = 0; i < values.Length; i++)
        {
            if (values[i] > 1 || values[i] < 0)
            {
                Debug.LogWarning(considerationRuntime[i].varibleNames + " is not between 0 -1");
            }
        }
        //Debug.Log("Before: " +name + ": " + values[0] + " : " + values[1]);
        //Debug.Log("After: " + name + ": " + responseCurve.Response(values[0], values[1]));


        return responseCurve.Response(values[0], values[1]);
    }

    void FindReference(Consideration consideration, Transform transform, int index)
    {
        GameObject obj = transform.root.Find(consideration.gameObjectName[index]).gameObject;
        if (obj == null)
        {
            obj = transform.root.gameObject;
        }

        connectedComponent[index] = obj.GetComponent(Type.GetType(consideration.componentName[index]));
        if (connectedComponent[index] == null)
        {
            connectedComponent[index] = obj.GetComponentInChildren(Type.GetType(consideration.componentName[index]));
            if (connectedComponent[index] == null)
            {
                Debug.LogError("Component (" + consideration.componentName[index] + ") not found on " + obj.name + " - Aborting");
                return;
            }
        }
        fieldInfos[index] = Type.GetType(consideration.componentName[index]).GetField(consideration.floatName[index].Split(' ')[1]);

        if (fieldInfos[index] == null)
        {
            properties[index] = Type.GetType(consideration.componentName[index]).GetProperty(consideration.floatName[index].Split(' ')[1]);
        }
        //Debug.Log(fieldInfos[index]);
        //Debug.Log(fieldInfos[index].GetValue(connectedComponent[index]));
        return;
        //connectedGameObjects[index] = obj;
        componentNames[index] = consideration.componentName[index];
        varibleNames[index] = consideration.floatName[index];

        //FieldInfo fieldType = connectedGameObjects[index].GetComponent(Type.GetType(componentNames[index])).GetType().GetField(varibleNames[index]);
        Type componentType = Type.GetType(componentNames[index]);
        //var component = connectedGameObjects[index].GetComponent(componentType);
        //Debug.Log(component.gameObject.GetComponent<Health>().GetCurrentHealth());
        FieldInfo newTry = componentType.GetField(varibleNames[index].Split(' ')[1]);
        Debug.Log(componentType + " " + varibleNames[index].Split(' ')[1] + " newTry: " + (newTry == null));
        //Debug.Log(newTry.GetValue(component));
        //Debug.Log(test.GetValue( connectedGameObjects[index].GetComponent(Type.GetType(componentNames[index])).GetType().GetField(varibleNames[index])));
        return;

        FieldInfo[] fields = connectedComponent.GetType().GetFields();
        Debug.Log(obj.GetComponent(Type.GetType(consideration.componentName[index])).GetType().GetField(consideration.floatName[index].Split(' ')[1]));
       // Debug.Log((connectecComponent.GetType().GetField(consideration.floatName[index] == null) + " tried: " +consideration.floatName[index].Split(' ')[1]);
        foreach (var thisVar in fields)
        {
            //Debug.Log(typeof(float) + " var: " + thisVar.FieldType);
            //Debug.Log(thisVar.GetValue(thisV))
            //if (thisVar.FieldType == typeof(float))
            //{
            //    Debug.Log("is true");
            //}
            Debug.Log(thisVar.FieldType);
            try
            {
                if (thisVar.FieldType == typeof(float))
                {
                    floats[index].Add(thisVar);
                }

            }
            catch (Exception e)
            {
                Debug.Log("We're down here");
                Debug.LogError(e);
            }
        }
        if (fields == null || floats[index].Count == 0)
        {
            Debug.LogError("No fields or no floats found in: " + consideration.componentName[index] + " - Aborting");
            return;
        }

        for (int i = 0; i < floats[index].Count; i++)
        {
            if (floats[index][i].ToString() == consideration.floatName[index])
            {
                variblePosition[index] = i;
                Debug.Log((float)floats[i][variblePosition[i]]);
                return;
            }
        }
        Debug.LogError("The correct float varible not found in: " + consideration.componentName[index] + " - Aborting");

    }



}
