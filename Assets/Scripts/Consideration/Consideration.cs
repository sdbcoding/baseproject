﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable]
[CreateAssetMenu(fileName = "Consideration", menuName = "DesireSystem/Consideration", order = 1)]
public class Consideration : ScriptableObject
{
#if UNITY_EDITOR
    
    public string DeveloperDescription = "";
#endif



    public bool UseConsideration1 = true;
    [HideInInspector]
    [SerializeField]
    public string[] gameObjectName = new string[2];
    [HideInInspector]
    [SerializeField]
    public string[] componentName = new string[2];
    [HideInInspector] [SerializeField]
    public string[] floatName = new string[2];

    [HideInInspector]

    public Consideration[] consideration = new Consideration[2];


    public bool UseConsideration2 = true;

    //Editor stuff
    [HideInInspector]
    [SerializeField]
    public GameObject[] obj = new GameObject[2];
    [HideInInspector]
    public Component[][] listComp = new Component[2][];
    [HideInInspector]
    public Component[] comp = new Component[2];
    [HideInInspector]
    public List<object>[] floats = new List<object>[2];


    //public GameObject[] prevGameObject = new GameObject[2];
    [HideInInspector]
    public string[][] optionsComponent = new string[2][];
    [HideInInspector]
    public int[] indexComponent = new int[2];

    //public Component[] prevComponent = new Component[2];
    [HideInInspector]
    public string[][] optionsFloats = new string[2][];
    [HideInInspector]
    public int[] indexFloats = new int[2];


    public ResponseCurve responseCurve;

    ////Editor stuff
    //public GameObject[] obj = new GameObject[2];
    //[HideInInspector]
    //public Component[][] listComp = new Component[2][];
    //[HideInInspector]
    //public Component[] comp = new Component[2];
    //[HideInInspector]
    //public List<object>[] floats = new List<object>[2];




}
