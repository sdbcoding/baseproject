﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterActionController : MonoBehaviour
{

    public NerveSystem nerveSystem;
    List<CharacterAction> actionsToPerform = new List<CharacterAction>();
    bool isInitiated;
#if UNITY_EDITOR
    [Multiline]
    public string ActionQue = "";
#endif

    public void StartNewActions(CharacterAction[] newActions)
    {
        ClearActionList();
        for (int i = 0; i < newActions.Length; i++)
        {
            if (newActions[i].isCloned)
            {
                actionsToPerform.Add(Instantiate(newActions[i]));
            }else
            {
                actionsToPerform.Add(newActions[i]);
            }
        }
    }

    void ClearActionList()
    {
        if (actionsToPerform.Count == 0) { return; }

        for (int i = actionsToPerform.Count - 1; i >= 1; i--)
        {
            if (actionsToPerform[i].isCancelable)
            {
                actionsToPerform.RemoveAt(i);
            }
        }

        if (actionsToPerform[0].isCancelable)
        {
            FinishCurrentAction();
        }
    }
    public void AddNewAction(CharacterAction newAction)
    {
        AddNewAction(newAction, actionsToPerform.Count);
    }

    void FinishCurrentAction()
    {
        actionsToPerform[0].FinishAction(nerveSystem);
        actionsToPerform.RemoveAt(0);
        isInitiated = false;

    }
    public void AddNewAction(CharacterAction newAction, int index)
    {
        if (index > actionsToPerform.Count)
        {
            Debug.Log("Index over action count, canceling");
            return;
        }
        if (index == actionsToPerform.Count)
        {
            actionsToPerform.Add(newAction);
            return;
        }

        List<CharacterAction> tempActions = new List<CharacterAction>();
        for (int i = index; i < actionsToPerform.Count; i++)
        {
            tempActions.Add(actionsToPerform[i]);
        }
        actionsToPerform.RemoveRange(index, actionsToPerform.Count - 1 - index);

        actionsToPerform.Add(newAction);

        for (int i = 0; i < tempActions.Count; i++)
        {
            actionsToPerform.Add(tempActions[i]);
        }
    }

    public void RunActions(float deltaTime)
    {
        if (actionsToPerform.Count == 0) { return; }

        if (!isInitiated)
        {
            actionsToPerform[0].InitiateAction(nerveSystem);
            isInitiated = true;
        }
        if (!actionsToPerform[0].DoAction(deltaTime,nerveSystem))
        {
            Debug.Log("Finishing: " + actionsToPerform[0].name);
            FinishCurrentAction();
        }
#if UNITY_EDITOR
        ActionQue = "";
        for (int i = 0; i < actionsToPerform.Count; i++)
        {
            ActionQue += actionsToPerform[i].name + "\n";
        }
#endif
    }

    

}
