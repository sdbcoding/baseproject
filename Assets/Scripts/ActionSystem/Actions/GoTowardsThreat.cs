﻿using UnityEngine;

[CreateAssetMenu(fileName = "MoveToThreat", menuName = "Actions/Movement/GoTowardsThreat", order = 0)]

public class GoTowardsThreat : CharacterAction
{
    public float minDistanceToThreat;
    public override bool DoAction(float deltaTime, NerveSystem nerveSystem)
    {
        if (!base.DoAction(deltaTime,nerveSystem))
        {
            return false;
        }
       //Debug.Log(nerveSystem.reasoning.BiggestThreat == null);
       nerveSystem.legs.SetDestination(nerveSystem.reasoning.BiggestThreat.transform.position);
        return true;
    }
    public override bool IsActionRelevant(NerveSystem nerveSystem)
    {

        if (nerveSystem.reasoning.BiggestThreat == null)
        {
            return false;
        }
        float distance = Vector3.Distance(nerveSystem.transform.position, nerveSystem.reasoning.BiggestThreat.transform.position);
        if (distance < minDistanceToThreat)
        {
            return false;
        }
        return true;
    }
}
