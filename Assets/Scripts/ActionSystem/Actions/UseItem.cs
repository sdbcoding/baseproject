﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ItemNamespace;

[CreateAssetMenu(fileName = "UseItem", menuName = "Actions/Action/UseItem", order = 3)]

public class UseItem : CharacterAction
{
    public bool UseItemClass = false;
    public ItemClass itemClassToUse;
    public Item itemToUse;
    public override bool DoAction(float deltaTime, NerveSystem nerveSystem)
    {
        if (!base.DoAction(deltaTime, nerveSystem))
        {
            return false;
        }
        if (UseItemClass)
        {
            nerveSystem.backPack.GetAndRemoveItemAtPosition(nerveSystem.backPack.GetPositionOfItem(itemClassToUse));
        }
        else
        {
            nerveSystem.backPack.GetAndRemoveItemAtPosition(nerveSystem.backPack.GetPositionOfItem(itemToUse));
        }
        return false;
    }
    public override bool IsActionRelevant(NerveSystem nerveSystem)
    {
        if (UseItemClass)
        {
            if (nerveSystem.backPack.Contains(itemClassToUse))
            {
                return true;
            }
        } else
        {
            if (nerveSystem.backPack.Contains(itemToUse))
            {
                return true;
            }

        }
        return false;
    }

}
