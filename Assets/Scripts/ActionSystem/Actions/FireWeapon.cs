﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FireWeapon", menuName = "Actions/Action/FireWeapon", order = 1)]

public class FireWeapon : CharacterAction
{
    public override bool DoAction(float deltaTime, NerveSystem nerveSystem)
    {
        if (!base.DoAction(deltaTime, nerveSystem))
        {
            return false;
        }
        nerveSystem.rightHand.FireWeaponCommand();
        return false;
    }

    public override bool IsActionRelevant(NerveSystem nerveSystem)
    {

        if (!nerveSystem.rightHand.isWeaponReady)
        {
            return false;
        }
        
        return true;
    }
}
