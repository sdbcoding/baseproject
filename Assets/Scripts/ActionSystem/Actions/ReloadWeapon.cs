﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Reload", menuName = "Actions/Action/ReloadWeapon", order = 0)]

public class ReloadWeapon : CharacterAction
{
    public override bool DoAction(float deltaTime, NerveSystem nerveSystem)
    {
        base.DoAction(deltaTime, nerveSystem);
        nerveSystem.reloadSystem.ReloadCommand();
        return false;
    }
}
