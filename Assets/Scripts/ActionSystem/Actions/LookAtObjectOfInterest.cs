﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LookAtObjectOfInterest", menuName = "Actions/Vision/LookAtObjectOfInterest", order = 1)]


public class LookAtObjectOfInterest : CharacterAction
{
    [Range(0, 180)]
    public float minAcceptableAngle = 5;
    public override bool DoAction(float deltaTime, NerveSystem nerveSystem)
    {
        if (!base.DoAction(deltaTime, nerveSystem))
        {
            return false;
        }
        nerveSystem.head.FocusOnObject(nerveSystem.memory.objectOfInterest);
        return true;
    }

    public override bool IsActionRelevant(NerveSystem nerveSystem)
    {

        if (nerveSystem.memory.objectOfInterest == null)
        {
            return false;
        }
        float angle = Vector3.Angle(nerveSystem.memory.objectOfInterest.transform.position - nerveSystem.head.transform.position, nerveSystem.head.transform.forward);

        if (Mathf.Abs(angle) > minAcceptableAngle)
        {
            return false;
        }
        return true;
    }
}
