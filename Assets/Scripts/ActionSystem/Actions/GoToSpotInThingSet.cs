﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GoToSpotInThingSet", menuName = "Actions/Movement/GoToSpotInThingSet", order = 3)]

public class GoToSpotInThingSet : CharacterAction
{
    public float minDistanceToSpot;
    public ThingSet setToGoTo;
    public bool ChooseRandomly = true;
    public override bool DoAction(float deltaTime, NerveSystem nerveSystem)
    {
        if (!base.DoAction(deltaTime, nerveSystem))
        {
            return false;
        }
        //Debug.Log(nerveSystem.reasoning.BiggestThreat == null);
        nerveSystem.legs.SetDestination(nerveSystem.memory.objectOfInterest.transform.position);
        return true;
    }
    public override bool IsActionRelevant(NerveSystem nerveSystem)
    {

        if (setToGoTo.GetItemListSize() == 0)
        {
            return false;
        }
        if (nerveSystem.memory.objectOfInterest == null)
        {
            if (ChooseRandomly)
            {
                nerveSystem.memory.objectOfInterest = setToGoTo.GetItemsAtIndex(Random.Range(0,setToGoTo.GetItemListSize())).gameObject;
            }
            else
            {
                nerveSystem.memory.objectOfInterest = setToGoTo.GetItemsAtIndex(0).gameObject;

            }
        }
        float distance = Vector3.Distance(nerveSystem.transform.position, nerveSystem.memory.objectOfInterest.transform.position);
        if (distance < minDistanceToSpot)
        {
            return false;
        }
        return true;
    }

    public override void FinishAction(NerveSystem nerveSystem)
    {
        base.FinishAction(nerveSystem);
        nerveSystem.memory.objectOfInterest = null;
    }
}
