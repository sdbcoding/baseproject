﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LookAtThreat", menuName = "Actions/Vision/LookAtThreat", order = 0)]

public class LookAtThreat : CharacterAction
{
    [Range(0,180)]
    public float minAcceptableAngle = 5;
    public override bool DoAction(float deltaTime, NerveSystem nerveSystem)
    {
        if (!base.DoAction(deltaTime, nerveSystem))
        {
            return false;
        }
        nerveSystem.head.FocusOnObject(nerveSystem.reasoning.BiggestThreat);
        return true;
    }

    public override bool IsActionRelevant(NerveSystem nerveSystem)
    {

        if (nerveSystem.reasoning.BiggestThreat == null)
        {
            return false;
        }
        if ( Mathf.Abs(nerveSystem.head.AngleToThreat) > minAcceptableAngle)
        {
            return false;
        }
        return true;
    }
}
