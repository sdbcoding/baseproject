﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharacterAction : ScriptableObject
{
    public bool isCancelable;
    public bool isCloned = false;
    public CharacterAction[] subActions;

    public virtual void InitiateAction(NerveSystem nerveSystem)
    {
        for (int i = 0; i < subActions.Length; i++)
        {
            subActions[i].InitiateAction(nerveSystem);
        }
    }

    //returns true if action should continue
    public virtual bool DoAction(float deltaTime)
    {
        if (!IsActionRelevant()) { return false; }

        for (int i = 0; i < subActions.Length; i++)
        {
            subActions[i].DoAction(deltaTime);
        }

        return true;
    }
    public virtual bool DoAction(float deltaTime, NerveSystem nerveSystem)
    {
        if (!IsActionRelevant(nerveSystem)) { return false; }

        for (int i = 0; i < subActions.Length; i++)
        {
            subActions[i].DoAction(deltaTime,nerveSystem);
        }

        return true;
    }

    public virtual void FinishAction(NerveSystem nerveSystem)
    {
        for (int i = 0; i < subActions.Length; i++)
        {
            subActions[i].FinishAction(nerveSystem);
        }
    }
    public virtual bool IsActionRelevant()
    {
        return true;
    }

    public virtual bool IsActionRelevant(NerveSystem nerveSystem)
    {
        return true;
    }
}
