﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadSystemReader : MonoBehaviour
{
    public ReloadSystem reloadSystem;
    bool isReloading = false;
    public FloatVariable currentTime;
    public FloatVariable reloadTime;
    public GameEvent StartReloading;
    public GameEvent FinishedReloading;
   

    // Update is called once per frame
    void Update()
    {
        currentTime.SetValue(reloadSystem.GetCurrentTime());
        reloadTime.SetValue(reloadSystem.GetReloadTime());
        if (isReloading != reloadSystem.GetIsReloading())
        {
            //Something have changed
            if (isReloading)
            {
                FinishedReloading.Raise();
            } else
            {
                StartReloading.Raise();
            }
        }
        isReloading = reloadSystem.GetIsReloading();
    }
}
