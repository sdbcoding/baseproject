﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowDownController : MonoBehaviour
{
    public FloatVariable objectTimeSpeed;
    public FloatReference minObjectTimeSpeed;
    public FloatReference acceleration;

    float currentSpeed =0;
    bool isSlowedDown = false;

    public bool IsSlowedDown { get => isSlowedDown; 
    set {
        isSlowedDown = value;
        currentSpeed = 0;
    }  }

    // Start is called before the first frame update
    void Start()
    {
        if (objectTimeSpeed == null)
        {
            Debug.LogError("No float variable added to SlowDownController, deleting");
            Destroy(this);
        }
        if (acceleration == 0)
        {
            Debug.LogWarning("Acceleration set to zero, no change in slow down time");
        }
        objectTimeSpeed.SetValue(1);
    }

    // Update is called once per frame
    void Update()
    {
        if (isSlowedDown)
        {
            if (objectTimeSpeed.GetValue() == minObjectTimeSpeed)
            {
                return;
            }
            currentSpeed += acceleration;
            objectTimeSpeed.ApplyChange(-currentSpeed);

            if (objectTimeSpeed.GetValue() < minObjectTimeSpeed)
            {
                objectTimeSpeed.SetValue(minObjectTimeSpeed);
                Debug.Log("lover than lowest");
            }
        } else {
            if (objectTimeSpeed.GetValue() == 1)
            {
                return;
            }
            currentSpeed += acceleration;
            objectTimeSpeed.ApplyChange(currentSpeed);
            if(objectTimeSpeed.GetValue() > 1)
            {
                objectTimeSpeed.SetValue(1);
            }

        }
    }
    public void ChangeIsSlowDown()
    {
        IsSlowedDown = !isSlowedDown;
    }
}
