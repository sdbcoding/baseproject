﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(CharacterController))]
public class Player2DMovement : OnUpdate {
	public Vector3Variable playerInput;
	public FloatReference yValue;
	float speed;
	public FloatReference acceleration;
	public FloatReference drag;
	public FloatReference maxSpeed;
	CharacterController controller;
	Vector3 currentDirection;
	Vector3 yCorrectedPlayerInput;
	Vector3 yCorrectedZero;
    private bool isAiming = false;
	bool isRunning = true;
	public bool withMagnitude = false;

    void Start()
	{
		yCorrectedZero = Vector3.zero;
		yCorrectedZero.y = yValue;
		currentDirection = yCorrectedZero;
		currentDirection = Vector3.zero;
		if (controller == null)
		{
			controller = GetComponent<CharacterController>();
		}
		
		
    }
	public override void UpdateEventRaised(float deltaTime)
	{
		if (!isRunning) {return;}
				
		yCorrectedPlayerInput = new Vector3(playerInput.Value.x,0,playerInput.Value.z);
		Vector3 directionFromCamera = Camera.main.ScreenToWorldPoint(yCorrectedPlayerInput);
		//yCorrectedPlayerInput = (Quaternion. directionFromCamera).normalized;
		//Debug.Log(yCorrectedPlayerInput + " <-before after -> " +transform.InverseTransformDirection(yCorrectedPlayerInput));
		CalculateCurrentDirection();
		CalculateCurrentSpeed();
		MovePlayerAccordingToVector3(deltaTime);
	}
	void CalculateCurrentSpeed()
	{
		float distance = Vector3.Distance(Vector3.zero,yCorrectedPlayerInput);
		//Debug.Log("distance: " + distance);
		if (distance == 0)
		{
			speed -= drag;
		}
		if (withMagnitude)
		{
			speed = maxSpeed;
		} else {
			speed += acceleration * distance;
		}
		if (speed > maxSpeed)
		{
			speed = maxSpeed;
		}
		if (speed < 0)
		{
			speed = 0;
		}
	}

	void CalculateCurrentDirection()
	{
		if (withMagnitude)
		{
			currentDirection = yCorrectedPlayerInput;
		} else {
			currentDirection += yCorrectedPlayerInput;
			currentDirection = currentDirection.normalized;
		}
	}

	void MovePlayerAccordingToVector3(float deltaTime)
	{
		if (currentDirection != Vector3.zero)
		{	
			//Debug.Log("Current direction: " + currentDirection);
			//Vector3 screenToWorld = Camera.main.ScreenToWorldPoint( currentDirection).normalized;
			//Debug.Log(screenToWorld);
			Vector3 newLookDirection =  currentDirection+ transform.position;
			newLookDirection.y = transform.position.y;
			transform.LookAt(newLookDirection);
			//transform.RotateAround(transform.position,Vector3.up,45);

			//transform.position += transform.forward * speed * Time.deltaTime;
            if (!isAiming)
            {
				float magniture = currentDirection.magnitude;
				//Debug.Log("Mag: " + magniture);
				if (withMagnitude)
				{
                	controller.Move(transform.forward * Mathf.Pow(speed * 2,magniture) * deltaTime);
				} else {
                	controller.Move(transform.forward * speed  * deltaTime);

				}

            }			
		}
	}

    public void StartAiming()
    {
        isAiming = true;
    }

    public void StopAiming()
    {
        isAiming = false;
    }

    
	public void PlayerHaveSpawned()
	{
		isRunning = true;

	}
    
}
