﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIState : OnUpdate
{
    //Relaxed is following normal patrol procedures. 
    //Alarmed is investigating something out of the ordinary. 
    //Alerted is spotting the player, or anything that will start combat
    //Combat activates the desire system.
    public enum FrameOfMind { Relaxed, Alarmed, Alerted, Combat};
    public enum State { Normal, Panicked, Stunned, Dead}
    public GameEvent enteringCombat;
    public NerveSystem nerveSystem;

    FrameOfMind currentFrameOfMind;

    State currentState;
    public float lastTimePanicked;

    //Temp
    AlertedReaction alertedReaction;

    public FrameOfMind CurrentFrameOfMind
    {
        get { return currentFrameOfMind; }
        set
        {
            Debug.Log("Changing state to: " + value);
            if (value == FrameOfMind.Combat)
            {
                enteringCombat.Raise();
            } else if (value == FrameOfMind.Alerted && currentFrameOfMind != FrameOfMind.Alerted)
            {
                if (alertedReaction == null)
                {
                    alertedReaction = gameObject.AddComponent<AlertedReaction>();
                    alertedReaction.Setup(nerveSystem);
                }
            }
            currentFrameOfMind = value;
            
        }
    }

    public State CurrentState
    {
        get { return currentState; }
        set
        {
            currentState = value;
            lastTimePanicked = GameTime.ElapsedTime;
        }
    }
   
}
