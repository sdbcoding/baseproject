﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ItemNamespace;
using WeaponNamespace;

public class Backpack : MonoBehaviour
{
    public List<Item> inventory;
    Item tempItem;
    private void Awake()
    {
        if (inventory == null)
        {
            inventory = new List<Item>();
        }
    }
    public bool Contains(Item item)
    {
        if (inventory.Contains(item))
        {
            return true;
        }
        return false;
    }
    public bool Contains(ItemClass itemClass)
    {
        for (int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].classType == itemClass)
            {
                return true;
            }
        }
        return false;
    }
    public int GetPositionOfItem(Item item)
    {
        for (int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i] == item)
            {
                return i;
            }
        }
        Debug.Log("Item not found in inventory, cannot return position");
        return 0;
    }
    public int GetPositionOfItem(ItemClass itemClass)
    {
        for (int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].classType == itemClass)
            {
                return i;
            }
        }
        Debug.Log("Item not found in inventory, cannot return position");
        return 0;
    }

    public Item GetAndRemoveItemAtPosition(int index)
    {
        if (index >= inventory.Count)
        {
            Debug.Log("Request for inventory item failed. Index too high");
            return null;
        }
        tempItem = inventory[index];
        inventory.RemoveAt(index);
        return tempItem;
    }
    
}
