﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NerveSystem : MonoBehaviour
{
    public Brain brain;
    public Eye eye;
    public Health health;

    public Head head;
    public Legs legs;
    public RightHand rightHand;
    public ReloadSystem reloadSystem;

    public Reasoning reasoning;
    public Memory memory;
    public AIState state;
    public Backpack backPack;

    [Header("Global References")]
    public ThingSet playerSet;

    [Header("Global Events")]
    public UpdateEvent coreUpdateEvent;
    public GameEvent playerSeen;
    public GameEvent playerSpotted;

    [Header("Character values")]
    public FloatReference timeUntilPlayerIsSpotted;
}
