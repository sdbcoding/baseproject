﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Legs : OnUpdate
{
    public NavMeshAgent agent;
    public Action movementDoneRespons;
    public FloatReference yValue;
    //Turning system
    protected bool isRotating = false;
    protected Vector3 direction;
    protected Quaternion startRotation;
    protected float angle;

    //Wait system references
    protected Vector3 savedPosition;
    protected bool isWaiting;
    protected float currentTime;
    protected float timeToReach;

    //Walkingbackwards
    protected bool isWalkingBackwards;
    protected Vector3 backWardsDestination;
    public float backwardsSpeed;
    protected float maxBackwardsTime = 2.0f;

    //Timeout system;
    protected float timeoutTimer;
    protected float maxTimeOut = 20.0f;


    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    public override void UpdateEventRaised(float deltaTime)
    {
        if (movementDoneRespons != null)
        {
            timeoutTimer += Time.deltaTime;
            if (timeoutTimer > maxTimeOut)
            {
                Debug.Log("Movement timeout reached");
                DestinationReached();
                timeoutTimer = 0;
                return;
            }
        }
        if (isWaiting)
        {
            currentTime += Time.deltaTime;
            if (currentTime > timeToReach)
            {
                isWaiting = false;
                SetDestination(savedPosition);
            }
            return;
        }
        if (isRotating)
        {
            //IMPLEMENT TURNING
            transform.localRotation = Quaternion.Lerp(startRotation, Quaternion.AngleAxis(angle, transform.up), currentTime / timeToReach);
            currentTime += Time.deltaTime;
            if (currentTime > timeToReach)
            {
                isRotating = false;
                DestinationReached();
            }

            return;
        }
        if (isWalkingBackwards)
        {
            currentTime += Time.deltaTime;
            Vector3 newPosition = Vector3.MoveTowards(transform.position, backWardsDestination, backwardsSpeed);
            newPosition.y = transform.position.y;
            transform.position = newPosition;
            Vector3 transformPositionForBackwards = transform.position;
            transformPositionForBackwards.y = yValue;

            float distanceForBackwards = Vector3.Distance(backWardsDestination, transformPositionForBackwards);
            if (distanceForBackwards <= 0.01f || distanceForBackwards == Mathf.Infinity)
            {
                isWalkingBackwards = false;
                DestinationReached();
            }
            if (currentTime > maxBackwardsTime)
            {
                isWalkingBackwards = false;
                DestinationReached();
            }
            return;
        }
        Vector3 agentDistination = agent.destination;
        agentDistination.y = yValue;
        Vector3 transformPosition = transform.position;
        transformPosition.y = yValue;
        float distance = Vector3.Distance(agentDistination, transformPosition);
        //Debug.Log(distance + " from:" +agentDistination +" and " + transformPosition);
        if (agent.isPathStale)
        {
            DestinationReached();
        }
        if (distance <= 0.01f || distance == Mathf.Infinity)
        {
            DestinationReached();
        }
    }
    void DestinationReached()
    {
        //Debug.Log("Destination reached");
        PauseNavAgent();
        if (movementDoneRespons != null)
        {
            Action tempAction = movementDoneRespons;
            movementDoneRespons = null;
            tempAction.Invoke();
        }

    }
    public virtual void SetDestination(Vector3 position)
    {
        ResumeNavAgent();
        agent.SetDestination(position);
        timeoutTimer = 0;
    }
    public virtual void SetDestination(Vector3 position, Action newResponse)
    {
        ResumeNavAgent();
        agent.SetDestination(position);
        NewReturnResponse(newResponse);
        timeoutTimer = 0;

    }
    public virtual void SetDestination(Vector3 position, Action newResponse, float timeBeforeBeginning)
    {
        isWaiting = true;
        timeToReach = timeBeforeBeginning;
        savedPosition = position;
        currentTime = 0;
        NewReturnResponse(newResponse);
        agent.SetDestination(transform.position);
        agent.isStopped = true;
        timeoutTimer = 0;

    }
    public virtual void TurnTowards(Vector3 newFacing, Action newResponse, float timeToTurn)
    {
        NewReturnResponse(newResponse);
        angle = Vector3.Angle(Vector3.forward, newFacing);
        isRotating = true;
        direction = newFacing;
        startRotation = transform.rotation;
        currentTime = 0;
        timeToReach = timeToTurn;
        timeoutTimer = 0;

    }
    void NewReturnResponse(Action newResponse)
    {
        if (movementDoneRespons != null)
        {
            Action tempAction = movementDoneRespons;
            movementDoneRespons = null;
            tempAction.Invoke();
        }
        movementDoneRespons = newResponse;
    }
    public virtual void WalkBackwards(float distance, Action newReponse)
    {
        //RaycastBackwards the distance. Walk to -X from what you hit if you hit anythingn
        NewReturnResponse(newReponse);
        currentTime = 0;
        isWalkingBackwards = true;
        RaycastHit hit;
        Physics.Raycast(transform.position, -transform.forward, out hit, distance);
        if (hit.collider == null)
        {
            backWardsDestination = (-transform.forward * distance) + transform.position;
        }
        else
        {
            float distanceToPoint = Vector3.Distance(transform.position, hit.point);
            backWardsDestination = (-transform.forward * (distanceToPoint - 0.1f)) + transform.position;
            Debug.Log(distanceToPoint);
        }
        backWardsDestination.y = yValue;
        //Debug.Log("Walking backwards to: " + backWardsDestination + " from: " +transform.position);
        timeoutTimer = 0;

    }

    public virtual void PauseNavAgent()
    {
        if (gameObject.activeSelf)
            agent.isStopped = true;
    }
    public virtual void ResumeNavAgent()
    {
        agent.isStopped = false;
    }
    public void ClearResponse(Action formerResponse)
    {
        if (Action.Equals(formerResponse, movementDoneRespons))
        {
            movementDoneRespons = null;
            //Debug.Log("Actions match, canceling");
        }
        else
        {
            //Debug.Log("Actions mismatch");

        }
    }
}
