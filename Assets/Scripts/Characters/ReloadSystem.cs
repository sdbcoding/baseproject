﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadSystem : OnUpdate
{
    public RightHand rightHand;
    public Backpack backpack;

    bool isReloading;
    float reloadTime;
    float currentTime;

    //Temp ref
    int missingBullets;
    int tempInventoryPosition;

    public bool BackPackContainsBulletForCurrentGun
    {
        get
        {
            return backpack.Contains(rightHand.bullet);
        }
    }

    public void ReloadCommand()
    {
        if (isReloading) { return; }
        if (BackPackContainsBulletForCurrentGun)
        {
            StartReloading();
        } else
        {
            NoMoreBulletsOfThatType();
        }
    }

    void StartReloading()
    {
        isReloading = true;
        reloadTime = rightHand.weaponInHand.magazine.reloadTime;
        currentTime = 0;
    }
    void FinishedReloading()
    {
        isReloading = false;
        LoadWeapon();
    }
    void LoadWeapon()
    {
        missingBullets = rightHand.weaponInHand.magazine.capacity - rightHand.numberOfBulletsInMagazine;
        for (int i = 0; i < missingBullets; i++)
        {
            if (backpack.Contains(rightHand.bullet))
            {
                tempInventoryPosition = backpack.GetPositionOfItem(rightHand.bullet);
                backpack.GetAndRemoveItemAtPosition(tempInventoryPosition);
                rightHand.numberOfBulletsInMagazine++;
            } else
            {
                NoMoreBulletsOfThatType();
                return;
            }
        }
    }
    public override void UpdateEventRaised(float deltaTime)
    {
        if (!isReloading) { return; }
        currentTime += deltaTime;
        if (currentTime > reloadTime)
        {
            FinishedReloading();
        }
    }
    public bool GetIsReloading()
    {
        return isReloading;
    }
   
    public float GetCurrentTime()
    {
        return currentTime;
    }
    public float GetReloadTime()
    {
        return reloadTime;
    }

    void NotFullyReloaded()
    {
        Debug.Log("Not enough bullets for a full reload");
    }
    void NoMoreBulletsOfThatType()
    {
        Debug.Log("Inventory contains no more bullets of that present type");
    }

}
