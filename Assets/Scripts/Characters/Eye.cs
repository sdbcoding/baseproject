﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eye : MonoBehaviour, OrganInterface
{
    [Header("Setup")]
    public Memory memory;

    [Header("Values")]
    public FloatReference visionAngle;
    public FloatReference visionLenght;
    public FloatReference yValue;
    public FloatReference characterColliderRadius;
    public LayerMask layerMaskToCheckIn;
    public ThingSet[] threatSets;
    public ThingSet[] setToLookFor;
    List<GameObject> threatSpotted;
    List<GameObject> objectsSpotted;
    List<GameObject> objectsSpottedButNoVisible;


    //temp
    float playerDistance;
    Vector3 raycastDestination;
    Ray ray;
    float angleToPlayer;
    RaycastHit hit;
    RaycastHit[] hits;
    Collider[] collidersInSphere;
    Thing[] thingsOnCollider;
    bool checkItemForImportance;




    public bool DoOrganFunction()
    {
        CheckLastKnowThreatPosition();
        CheckForOtherThingSets();

        //Will be implemented when items can be dropped
        //CheckVision();
        return true;
    }

    void CheckLastKnowThreatPosition()
    {
        for (int i = memory.lastKnownThreatPosition.Count -1; i >= 0; i--)
        {
            playerDistance = Vector3.Distance(memory.lastKnownThreatPosition[i], transform.position);
            if (playerDistance < visionLenght)
            {
                raycastDestination = transform.position;
                raycastDestination.y = yValue;
                ray.origin = raycastDestination;

                //Check for center
                raycastDestination = memory.LastKnownThreatPosition;
                ray.direction = (raycastDestination - ray.origin).normalized;

                //Check to see if player is within the angle
                angleToPlayer = Vector3.Angle(transform.forward, ray.direction);
                if (angleToPlayer < visionAngle)
                {
                    Physics.Raycast(ray, out hit, visionLenght, layerMaskToCheckIn);
                    if (hit.collider == null)
                    {
                        //Players last known location is seen, and the player is not there.
                        memory.CheckedLastKnownPosition(i);
                    }
                }
            }

        }
    }
    /*
    void CheckForPlayer()
    {
        playerDistance = Vector3.Distance(playerTransform.position, transform.position);
        if (playerDistance < visionLenght)
        {
            
            //Do three raycasts, left right and center
            //Find the line that goes 
            raycastDestination = transform.position;
            raycastDestination.y = playerTransform.position.y;
            ray.origin = raycastDestination;

            //Check for center
            raycastDestination = playerTransform.position;
            ray.direction = (raycastDestination - ray.origin).normalized;

            //Check to see if player is within the angle
            angleToPlayer = Vector3.Angle(transform.forward, ray.direction);
            //Debug.Log("Angle: " + angleToPlayer);
            memory.angleToThreat = angleToPlayer;
            if (angleToPlayer < visionAngle)
            {
                Physics.Raycast(ray, out hit, visionLenght, layerMaskToCheckIn);
                if (hit.collider != null)
                {
                    if (CheckColliderForThingSet(hit.collider,threatSets))
                    {
                        memory.LastKnownPlayerPosition = playerTransform.position;
                        return;
                    }
                }

                //Check for left
                raycastDestination = playerTransform.position + (-transform.right * characterColliderRadius);
                ray.direction = (raycastDestination - ray.origin).normalized;
                Physics.Raycast(ray, out hit, visionLenght, layerMaskToCheckIn);
                if (hit.collider != null)
                {
                    if (CheckColliderForThingSet(hit.collider, threatSets))
                    {
                        memory.LastKnownPlayerPosition = playerTransform.position;
                        return;
                    }
                }

                
                //Check to the right
                raycastDestination = playerTransform.position + (transform.right * characterColliderRadius);
                ray.direction = (raycastDestination - ray.origin).normalized;
                Physics.Raycast(ray, out hit, visionLenght, layerMaskToCheckIn);
                if (hit.collider != null)
                {
                    if (CheckColliderForThingSet(hit.collider, threatSets))
                    {
                        memory.LastKnownPlayerPosition = playerTransform.position;
                        return;
                    }
                }

            }
        }
        //Debug.Log("Did not find player");
        memory.isThreatVisible = false;
    }
    bool CheckColliderForThingSet(Collider collider, ThingSet thingSet)
    {
        //Debug.Log("Hit collider:" + collider.gameObject.name);

        thingsOnCollider = collider.GetComponents<Thing>();
        if (thingsOnCollider.Length == 0)
        {
            return false;
        }
        for (int j = 0; j < thingsOnCollider.Length; j++)
        {
            if (ThingSet.Equals(thingsOnCollider[j].RunTimeSet, thingSet))
            {
                return true;
            }
        }
        return false;
    }
    */
    void CheckForOtherThingSets()
    {
        //Debug.Log(transform.forward);
        Vector3 forward = transform.forward;
        collidersInSphere = Physics.OverlapSphere(transform.position, visionLenght, -1, QueryTriggerInteraction.Collide);

        if (collidersInSphere.Length == 0)
        {
            Debug.Log("No colliders within sphere");
            return;
        }



        threatSpotted = new List<GameObject>();
        objectsSpotted = new List<GameObject>();
        //objectsSpottedButNoVisible = new List<GameObject>();

        for (int i = 0; i < collidersInSphere.Length; i++)
        {

            //Debug.Log(collidersInSphere[i].gameObject.name);

            //Checks if the found collider is withint th e
            //float angle = Vector3.Angle(transform.forward,(collidersInSphere[i].transform.position -transform.position).normalized);
            float angle;
            Vector3 raycastDirection;
            Vector3 yCorrectedSpherePosition = collidersInSphere[i].transform.position;
            yCorrectedSpherePosition.y = transform.position.y;

            raycastDirection = yCorrectedSpherePosition - transform.position;
            angle = Vector3.Angle(transform.forward, raycastDirection.normalized);

            //Debug.Log("Angle of Collider; " + angle + " from object: " + collidersInSphere[i].transform.gameObject.name);
            if (Mathf.Abs(angle) > visionAngle.Value / 2)
            {
                continue;
            }
            Thing[] thingsOnCollider = collidersInSphere[i].transform.GetComponents<Thing>();
            if (thingsOnCollider.Length == 0)
            {
                continue;
            }
            for (int j = 0; j < thingsOnCollider.Length; j++)
            {
                checkItemForImportance = true;
                for (int k = 0; k < threatSets.Length; k++)
                {
                    if (ThingSet.Equals(thingsOnCollider[j].RunTimeSet, threatSets[k]))
                    {
                        //TODO Potentioally, update this so it only checks if it hits something that could block vision.
                        //Debug.Log("Trying for: " + collidersInSphere[i].gameObject.name);
                        RaycastHit hit;
                        Vector3 newRaycastPosition = transform.position;
                        newRaycastPosition.y = 0.5f;
                        Physics.Raycast(newRaycastPosition, raycastDirection, out hit, 50);

                        if (hit.collider == null)
                        {
                            continue;
                        }
                        if (GameObject.Equals(hit.collider.gameObject, collidersInSphere[i].gameObject))
                        {
                            threatSpotted.Add(collidersInSphere[i].gameObject);
                            k = threatSets.Length;
                            checkItemForImportance = false;
                            break;
                        }
                        //else
                        //{
                        //    objectsSpottedButNoVisible.Add(collidersInSphere[i].gameObject);
                        //}

                    }
                }
                if (checkItemForImportance)
                {
                    for (int k = 0; k < setToLookFor.Length; k++)
                    {
                        if (ThingSet.Equals(thingsOnCollider[j].RunTimeSet, setToLookFor[k]))
                        {
                            //TODO Potentioally, update this so it only checks if it hits something that could block vision.
                            //Debug.Log("Trying for: " + collidersInSphere[i].gameObject.name);
                            RaycastHit hit;
                            Vector3 newRaycastPosition = transform.position;
                            newRaycastPosition.y = 0.5f;
                            Physics.Raycast(newRaycastPosition, raycastDirection, out hit, 50);

                            if (hit.collider == null)
                            {
                                continue;
                            }
                            if (GameObject.Equals(hit.collider.gameObject, collidersInSphere[i].gameObject))
                            {
                                objectsSpotted.Add(collidersInSphere[i].gameObject);
                                k = threatSets.Length;
                            }
                            //else
                            //{
                            //    objectsSpottedButNoVisible.Add(collidersInSphere[i].gameObject);
                            //}

                        }
                    }
                }
            }

            //----------------
            memory.VisibleThreats = threatSpotted.ToArray();
            memory.VisibleGameObjects = objectsSpotted.ToArray();
        }
    }
}
