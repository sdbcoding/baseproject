﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlertedReaction : OnUpdate
{
    Head head;
    AIState aiState;
    Memory memory;
    Legs legs;
    FloatReference timeUntilPlayerSpotted;
    float currentTime;


    //Temp
    float angleToLook;
    public void Setup(NerveSystem nerveSystem)
    {
        updateEvent = nerveSystem.coreUpdateEvent;
        updateEvent.RegisterListener(this);
        head = nerveSystem.head;
        aiState = nerveSystem.state;
        memory = nerveSystem.memory;
        legs = nerveSystem.legs;
        timeUntilPlayerSpotted = nerveSystem.timeUntilPlayerIsSpotted;
        head.FocusOnObject(nerveSystem.playerSet.GetItemsAtIndex(0).gameObject);

    }

    public override void UpdateEventRaised(float deltaTime)
    {
        //if (memory.isThreatVisible)
        //{
        //    DoReaction(deltaTime);
        //} else
        //{
        //    RetractReaction(deltaTime);
        //}

    }

    public void RetractReaction(float deltaTime)
    {
        currentTime -= deltaTime;
        if (currentTime < 0)
        {
            aiState.CurrentFrameOfMind = AIState.FrameOfMind.Alarmed;
            DestroyThis();
        }
    }

    public void DoReaction(float deltaTime)
    {
        currentTime += deltaTime;
        if (!head.IsLookingAt(memory.LastKnownThreatPosition))
        {
            //Debug.Log("calling");
        } 
        legs.PauseNavAgent();
        if (currentTime > timeUntilPlayerSpotted)
        {
            aiState.CurrentFrameOfMind = AIState.FrameOfMind.Combat;
            DestroyThis();
        }
    }

    void DestroyThis()
    {
        Destroy(this);
    }
}
