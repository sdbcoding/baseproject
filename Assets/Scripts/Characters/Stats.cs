﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Stats : OnUpdate
{
    public FloatReference startingAccuracy;

    float[] timeOfAdditionToShort;
    public FloatReference timeBeforeDecrease;
    public FloatReference decreaseSpeed;

    float[] shortValue;

    float accuracy;
    float shortAccuracy;

    public float desireToStandStill = 0.2f;

    public float ShortAccuracy { get { return shortValue[0]; }
        set
        {
            shortValue[0] += value;
            if (shortValue[0] < -accuracy)
            {
                shortValue[0] = -accuracy;
            }
            timeOfAdditionToShort[0] = GameTime.ElapsedTime;
        } }
    public float Accuracy { get { return Math.Max(0, accuracy + shortValue[0]); }
        set {
            accuracy += value;
        } }


    //Temp
    float timeDifference;

    private void Start()
    {
        timeOfAdditionToShort = new float[1];
        shortValue = new float[1];

        accuracy = startingAccuracy;
    }


    public override void UpdateEventRaised(float deltaTime)
    {
        DecreaseShort(deltaTime);
    }

    void DecreaseShort(float deltaTime)
    {
        for (int i = 0; i < timeOfAdditionToShort.Length; i++)
        {
            if (shortValue[i] == 0) continue;
            timeDifference = GameTime.ElapsedTime - timeOfAdditionToShort[i];
            if (timeDifference > timeBeforeDecrease)
            {
                if (shortValue[i] > 0)
                {
                    shortValue[i] -= decreaseSpeed * deltaTime;
                    if (shortValue[i] < 0)
                    {
                        shortValue[i] = 0;
                    }
                } else
                {
                    shortValue[i] += decreaseSpeed * deltaTime;
                    if (shortValue[i] > 0)
                    {
                        shortValue[i] = 0;
                    }
                }
            }
        }
    }
}
