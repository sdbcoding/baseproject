﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Memory : MonoBehaviour
{
    //Eye
    private float somethingEntirelyDifferent;
    private bool isThreatVisible;
    public bool IsThreatVisible
    {
        get
        {
            //Debug.Log("Threats: " + VisibleThreats.Length);
            if (VisibleThreats.Length != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set => isThreatVisible = value;
    }

    [HideInInspector]
    public float relevantWeaponDistance;

    //--World objects seen
    [HideInInspector]
    private GameObject[] visibleGameObjects = new GameObject[0];
    public GameObject[] VisibleGameObjects { get => visibleGameObjects; set { KnownObjects = new List<GameObject>(value); visibleGameObjects = value; } }

    List<GameObject> knownObjects = new List<GameObject>();
    public List<GameObject> KnownObjects
    {
        get => knownObjects;
        set
        {
            for (int i = knownObjects.Count-1; i >= 0; i--)
            {
                int shouldBeAdded = -1;
                for (int j = 0; j < value.Count; j++)
                {
                    if (value[j] == knownObjects[i])
                    {
                        shouldBeAdded = j;
                        break;
                    }
                }
                if (shouldBeAdded != -1)
                {
                    knownObjects.Add(value[shouldBeAdded]);
                }
            }
        }
    }

    public GameObject objectOfInterest;

    //--ThreatSystem
    [HideInInspector]
    GameObject[] visibleThreats = new GameObject[0];
    [HideInInspector]
    public List<float> lastKnownThreatTime = new List<float>();
    [HideInInspector]
    public List<Vector3> lastKnownThreatPosition = new List<Vector3>();
    [HideInInspector]
    public List<GameObject> lastKnowThreatObj = new List<GameObject>();
    public void CheckedLastKnownPosition(int index)
    {
        lastKnownThreatPosition.RemoveAt(index);
        lastKnowThreatObj.RemoveAt(index);
        lastKnownThreatTime.RemoveAt(index);
    }
    public GameObject[] VisibleThreats { get => visibleThreats;
        set
        {
            for (int i = 0; i < visibleThreats.Length; i++)
            {
                bool isItGone = true;
                for (int j = 0; j < value.Length; j++)
                {
                    if (visibleThreats[i] == value[j])
                    {
                        isItGone = false;
                        continue;
                    } else
                    {
                        for (int k = 0; k < lastKnowThreatObj.Count; k++)
                        {
                            if (value[j] == lastKnowThreatObj[k])
                            {
                                lastKnownThreatPosition.RemoveAt(k);
                                lastKnowThreatObj.RemoveAt(k);
                                lastKnownThreatTime.RemoveAt(k);
                                break;
                            }
                        }
                    }
                }
                if (isItGone)
                {
                    lastKnownThreatPosition.Add(visibleThreats[i].transform.position);
                    lastKnowThreatObj.Add(visibleThreats[i]);
                    lastKnownThreatTime.Add(GameTime.ElapsedTime);
                }
            }
            visibleThreats = value;
        }
    }
    [HideInInspector]
    public List<float> timesABulletWentCloseBy = new List<float>();

    public void AddBullet()
    {
        timesABulletWentCloseBy.Add(GameTime.ElapsedTime);
    }

    private void Awake()
    {
        timesABulletWentCloseBy = new List<float>();
    }

    public bool VisiblObjectsContains(GameObject obj)
    {
        for (int i = 0; i < VisibleGameObjects.Length; i++)
        {
            if (GameObject.Equals(VisibleGameObjects[i],obj))
            {
                return true;
            }
        }
        return false;
    }

    public Vector3 LastKnownThreatPosition
    {
        get
        {
            if (lastKnownThreatPosition.Count == 0)
            {
                return Vector3.zero;
            }
            return lastKnownThreatPosition[lastKnownThreatPosition.Count-1];
        }
        
    }


   

}
