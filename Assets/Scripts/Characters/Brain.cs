﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brain : OnUpdate
{
    [Header("Setup")]
    public AIState aiState;
    public Memory memory;
    public DesireController desireController;
    public CharacterActionController characterActionController;

    bool prevIsPlayerSpotted = false;

    public OrganInterface[] organInterfaces;

    private void Start()
    {
        organInterfaces = transform.GetComponentsInChildren<OrganInterface>();
    }

    public override void UpdateEventRaised(float deltaTime)
    {
        RunOrgans(deltaTime);
        RunDesires(deltaTime);
        RunActions(deltaTime);
    }

    void RunOrgans(float deltaTime)
    {
        for (int i = 0; i < organInterfaces.Length; i++)
        {
            organInterfaces[i].DoOrganFunction();
        }
    }

    void RunDesires(float deltaTime)
    {
        desireController.RunDesires(deltaTime);
    }
    
    void RunActions(float deltaTime)
    {
        characterActionController.RunActions(deltaTime);
    }
    
}
