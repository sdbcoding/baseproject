﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Head : OnUpdate
{
    public Memory memory;
    public NerveSystem nerveSystem;
    public Transform enemyBody;

    public FloatReference maxAngle;
    public FloatReference headSpeed;
    float currentTime;
    float timeToLookThere;
    float timeLookingThere;
    Vector3 lookAtPosition;
    Vector3 lookAtDirection;
    GameObject focusObject;
    float angleToTurnTo;
    Action turningDoneResponse;
    bool isLookingThere = false;
    Quaternion startRotation;

    bool isFocussing = false;
    bool isNormalBehaviour = true;
    void Awake()
    {
        isNormalBehaviour = true;
    }
    public bool IsLookingAt(Vector3 position)
    {
        if (!isNormalBehaviour)
        {
            if (Vector3.Equals(position,lookAtPosition))
            {
                return true;
            }
        }
        return false;
    }
    public bool IsFocusingAt(GameObject obj)
    {
        if (!isNormalBehaviour)
        {
            if (GameObject.Equals(obj, focusObject))
            {
                return true;
            }
        }
        return false;
    }
    public virtual void ClearResponse(Action formerResponse)
    {
        if (Action.Equals(formerResponse, turningDoneResponse))
        {
            turningDoneResponse = null;
            //Debug.Log("Actions match, canceling");
        }
        else
        {
            //Debug.Log("Actions mismatch");

        }
    }

    public virtual void ReturnToNormal()
    {
        angleToTurnTo = Vector3.Angle(transform.worldToLocalMatrix * transform.forward, Vector3.forward);
        //Debug.Log("New angle to look at: " + angleToTurnTo);
        isLookingThere = false;
        isFocussing = false;
        isNormalBehaviour = true;
    }
    public virtual void LookAt(float angle, float timeUntil, float timeThere, Action responseWhenDone)
    {
        isFocussing = false;

        if (Mathf.Abs(angle) > maxAngle)
        {
            Debug.Log("Angle provided is above maxAngle, canceling");
            angle = maxAngle;
        }
        //TODO Implement yValue;
        float angleOffset = Vector3.Angle(transform.forward, enemyBody.forward);

        //angleToTurnTo = angle + angleOffset;
        angleToTurnTo = angle;
        //Debug.Log(angleToTurnTo +" med angleoffset: " +angleOffset + " transform.forward: " + transform.forward + " enemyBody: " + enemyBody.forward);
        isNormalBehaviour = false;
        isFocussing = false;
        currentTime = 0;
        timeToLookThere = timeUntil;
        timeLookingThere = timeThere;
        startRotation = transform.localRotation;
        //Debug.Log("Start rotation: " + startRotation);
        if (turningDoneResponse != null)
        {
            turningDoneResponse.Invoke();
        }
        turningDoneResponse = responseWhenDone;
    }
    

    public void LookAt(Vector3 position)
    {
        //Debug.Log("LookAt command with: " + position);
        lookAtPosition = position;
        position.y = transform.position.y;
        Vector3 directionToEndAt = (position - transform.position).normalized;
        //Debug.DrawRay(transform.position, directionToEndAt, Color.red, 5.0f);
        float angleToTurn = Vector3.SignedAngle(transform.forward, directionToEndAt,Vector3.up);
        //Debug.Log("Angle to turn to: " + angleToTurn +" other calc: " + Vector3.Angle(transform.forward,directionToEndAt) + " forward: " + transform.forward);
        float maxTimeToLookTheWholeWay = 1.5f;
        float timeToLook = maxTimeToLookTheWholeWay * (Math.Abs(angleToTurn) / maxAngle);
        LookAt(angleToTurn, timeToLook, 2, null);
    }
    void LookingDone()
    {
        Debug.Log("Looking done");
        ReturnToNormal();
        if (turningDoneResponse != null)
        {
            Action tempAction = turningDoneResponse;
            turningDoneResponse = null;
            tempAction.Invoke();
        }
    }

    public override void UpdateEventRaised(float deltaTime)
    {

        if (isFocussing)
        {
            Focussing(deltaTime);
            return;
        }
        if (isNormalBehaviour)
        {
            NormalBehaviour(deltaTime);
        }
        else
        {
            //Debug.Log("Small test");
            LookTowards();
        }
    }
    void Focussing(float deltaTime)
    {

        if (!memory.VisiblObjectsContains(focusObject))
        {
            ReturnToNormal();
            return;
        }
        //find angle, if too large take closest to max.
        Vector3 tempLookPosition = focusObject.transform.position;
        tempLookPosition.y = transform.position.y;
        float angle = Vector3.SignedAngle(transform.forward, (transform.position - tempLookPosition).normalized, transform.up);
        //Debug.Log("Is focussing: " + angle);

        /*Debug.Log("Focusing angle: " + angle);
        if (angle < -maxAngle)
        {
            angle = -maxAngle;
        }
        else if (angle > maxAngle)
        {
            angle = maxAngle;
        }
        */
        if (Mathf.Abs(angle) < 178.0f)
        {
            transform.Rotate(transform.up, -Math.Sign(angle) * headSpeed * deltaTime);
        }
        //transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.AngleAxis(angle, transform.up),deltaTime);
    }
    public virtual void FocusOnPosition(Vector3 position)
    {
        isFocussing = true;
        lookAtDirection = position;
    }
    public virtual void FocusOnObject(GameObject objectToFocusOn)
    {
        isFocussing = true;
        focusObject = objectToFocusOn;
    }
    void LookTowards()
    {
        currentTime += Time.deltaTime;
        if (isLookingThere)
        {
            if (timeLookingThere < currentTime)
            {
                LookingDone();
                //Return to normal?
                return;
            }
        }
        else
        {
            //Debug.Log("Looking there: " + angleToTurnTo + " starting from: " +startRotation + " quaternion thing :" + Quaternion.AngleAxis(angleToTurnTo, transform.up));

            transform.localRotation = Quaternion.Lerp(startRotation, Quaternion.AngleAxis(angleToTurnTo, transform.up), currentTime / timeToLookThere);
            Debug.Log("Angle to turn to: " + angleToTurnTo +" vs" +  Quaternion.Angle(startRotation, Quaternion.AngleAxis(angleToTurnTo, transform.up)) + " Enemybody: " + enemyBody.transform.forward);
            
            //do a test if you are currently looking in the correct direction and stop
            Vector3 tempVector = lookAtPosition;
            tempVector.y = transform.position.y;
            Vector3 directionToEndAt = (tempVector - transform.position).normalized;
            float testAngle = Vector3.Angle(transform.forward, directionToEndAt);

            if (timeToLookThere < currentTime || testAngle < 2)
            {
                isLookingThere = true;
                currentTime = 0;
                return;
            }
        }
    }
    void NormalBehaviour(float deltaTime)
    {
        //Wait for game designer
        transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.AngleAxis(angleToTurnTo, transform.up), headSpeed * deltaTime);
    }

    public float AngleToThreat
    {
        get
        {
            GameObject temp = nerveSystem.reasoning.ClosestThreat();
            if (temp == null)
            {
                return 360;
            }
            return Vector3.Angle(nerveSystem.head.transform.forward, (temp.transform.position - nerveSystem.transform.position).normalized);

        }
    }
}
