﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ItemNamespace;

public class RightHand : MonoBehaviour
{
    //References outside of weapon
    public Stats stats;
    //weapon equiped
    public Weapon weaponInHand;
    private bool weaponReady;
    public bool isWeaponReady { get
        {
            if (weaponInHand != null && numberOfBulletsInMagazine != 0)
            {
                //Debug.Log("Ready: " + true);
                return true;
            }
            //Debug.Log("Ready: " + false);

            return false;
        }
        set => weaponReady = value; }

    //Ref needed to fire weapon
    public WeaponCockingSystem cockingSystem;
    public SpawnBullet spawnBullet;
    //References needed to setup the bullet
    GameObject tempBullet;

    //What is loaded into the weapon
    public Bullet bullet;
    public int numberOfBulletsInMagazine;
    private float magazineEmptiness;
    public float MagazineEmptiness
    {
        get
        {
            if (weaponInHand == null)
            {
                return 0;
            }
            //Debug.Log("Emptiness: " + (1.0f - ((float)numberOfBulletsInMagazine / weaponInHand.magazine.capacity)));
            return 1.0f- ((float)numberOfBulletsInMagazine / weaponInHand.magazine.capacity);
        }
    }

    //temp
    float startingAngle;
    float angle;
    float accuracyDeviation;


    public void FireWeaponCommand()
    {
        if (weaponInHand == null)
            return;
        CheckIfWeaponIsCocked();
    }
    void CheckIfWeaponIsCocked()
    {
        if (cockingSystem.IsWeaponCocked)
        {
            CheckIfWeaponHaveBullets();
        }
        else
        {
            weaponNotCocked();
        }
    }
    void CheckIfWeaponHaveBullets()
    {
        if (numberOfBulletsInMagazine != 0)
        {
            FireWeapon();
        } else
        {
            OutOfBullets();
        }
    }
    void OutOfBullets()
    {
        //Do something here
        //Debug.Log("Out of bullets");
    }

    void weaponNotCocked()
    {
        //Play fail sound
        //Debug.Log("Weapon not cocked");

    }
    void FireWeapon()
    {

        //Find the spread and the starting angle
        if (weaponInHand.muzzle.spread == 0)
        {
            startingAngle = 0;
        }
        else
        {
            startingAngle = -(weaponInHand.muzzle.spread / 2);

        }
        //Calculate accuracy deviation
        accuracyDeviation = UnityEngine.Random.Range(0, 100 - stats.Accuracy);
        accuracyDeviation -= (100 - stats.Accuracy) /  2;
        //Debug.Log(accuracyDeviation + " " + stats.Accuracy);
        startingAngle += accuracyDeviation;

        angle = weaponInHand.muzzle.spread / weaponInHand.muzzle.numberOfBulletsSpawned;
        //Debug.Log("Transform.up: " + transform.up);
        for (int i = 0; i < weaponInHand.muzzle.numberOfBulletsSpawned; i++)
        {
            if(numberOfBulletsInMagazine == 0)
            {
                OutOfBullets();
                continue;
            }
            tempBullet = spawnBullet.SpawnBulletCommand(startingAngle + (angle * i));
            SetupBullet(tempBullet);
            numberOfBulletsInMagazine--;
        }
        stats.ShortAccuracy = -weaponInHand.cockingMechanism.recoil;
    }
    void SetupBullet(GameObject bulletObj)
    {
        bulletObj.GetComponent<BulletContainer>().SetupBullet(bullet,weaponInHand.cockingMechanism);
    }

    public void Reloading(Bullet newBullet,int numberOfBullets)
    {
        numberOfBulletsInMagazine = numberOfBullets;
        bullet = newBullet;
    }
}
