﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reasoning : MonoBehaviour
{
    public Memory memory;
    public NerveSystem nerveSystem;

    //Sense of danger
    float timeBackWhereCloseBulletsMatter = 1.0f;

    //temp
    float senseOfDanger;
    float timeSince;
    float closestDistance;
    int gameObjectPosition;
    float distance;

    public float SenseOfDanger
    {
        get
        {
            senseOfDanger = 1;
            for (int i = memory.timesABulletWentCloseBy.Count - 1; i >= 0; i--)
            {
                timeSince = GameTime.ElapsedTime - memory.timesABulletWentCloseBy[i];
                if (timeBackWhereCloseBulletsMatter < timeSince)
                {
                    memory.timesABulletWentCloseBy.RemoveAt(i);
                    continue;
                }
            }
            senseOfDanger -= 0.1f * memory.timesABulletWentCloseBy.Count;
            if (senseOfDanger < 0)
            {
                senseOfDanger = 0;
            }
            return senseOfDanger;
        }
    }

    private GameObject biggestThreat;

    public GameObject BiggestThreat {
        get
        {
            if (biggestThreat != null)
            {
                return biggestThreat;
            }
            GameObject temp = ClosestThreat();
            if (temp == null)
            {
                temp = LastKnownThreat();
            }
            return temp;
        }
        set => biggestThreat = value; }

    public GameObject LastKnownThreat()
    {
        if (memory.LastKnownThreatPosition != null)
        {
            return memory.lastKnowThreatObj[memory.lastKnowThreatObj.Count - 1];
        }
        return null;
    }

    public GameObject ClosestThreat()
    {
        if (!memory.IsThreatVisible)
        {
            return null;
        }
        closestDistance = 0;
        gameObjectPosition = 0;
        for (int i = 0; i < memory.VisibleThreats.Length; i++)
        {
            distance = Vector3.Distance(memory.VisibleThreats[i].transform.position, transform.position);
            if (distance < closestDistance)
            {
                gameObjectPosition = i;
                closestDistance = distance;
            }
        }
        return memory.VisibleThreats[gameObjectPosition];

    }

    public bool IsThreatWithInRange
    {
        get
        {
            GameObject temp = ClosestThreat();
            if (temp == null)
            {
                return false;
            }
            if (!nerveSystem.rightHand.isWeaponReady)
            {
                return false;
            }
            float distance = Vector3.Distance(temp.transform.position, transform.position);
            if (distance > nerveSystem.rightHand.weaponInHand.weaponType.relevantRange)
            {
                return false;
            }
            return true;
        }
    }


    public float AngleToEnemyInPercentageFrom180
    {
        get
        {
            float percent =  1 - (Mathf.Abs( nerveSystem.head.AngleToThreat) / 180);
            //Debug.Log(percent + " from: " + nerveSystem.head.AngleToThreat);
            if (percent < 0.0f)
            {
                return 0.0f;
            }
            return percent;
        }
    }

}
