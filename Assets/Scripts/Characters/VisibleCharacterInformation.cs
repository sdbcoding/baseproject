﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ItemNamespace;

public class VisibleCharacterInformation : MonoBehaviour
{
    public RightHand rightHand;
    public Health health;
    public bool IsHurt()
    {
        if (health.HealthProcentage < 1.0f)
        {
            return false;
        }
        return true;
    }

    public Weapon GetWeaponInfo()
    {
        return rightHand.weaponInHand;
    }
}
