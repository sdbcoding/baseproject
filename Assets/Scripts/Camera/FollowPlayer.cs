﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : OnUpdate
{
    [Range(0, 100)]
    public float followSpeed = 99;
    public float yValue = 22;

    public ThingSet playerSet;       //Public variable to store a reference to the player game object
    Transform transforfollowing;
    bool isFollowingPlayer = true;

    // Use this for initialization
    void Start()
    {
        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        //offset = transform.position - player.transform.position;
        //Debug.Log(offset);
        transforfollowing = playerSet.GetItems()[0].transform;
    }

    // LateUpdate is called after Update each frame
    public override void UpdateEventRaised(float deltaTime)
    {
        if (!isFollowingPlayer) { return; }
        Vector3 newPosition = Vector3.Lerp(transform.position, transforfollowing.position, 1 - Mathf.Pow(1 - followSpeed * .01f, deltaTime));
        float newYValue = transform.position.y;
        if (newYValue < yValue)
        {
            newYValue = transform.position.y + 0.1f;

        }
        transform.position = new Vector3(newPosition.x, newYValue, newPosition.z);
        // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
        //transform.position = player.transform.position + offset;
    }
    
    public void PlayerSpawn()
    {
        isFollowingPlayer = true;
    }
}
