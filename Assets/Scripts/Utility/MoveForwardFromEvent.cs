﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForwardFromEvent : OnUpdate
{
    public FloatReference speed;
    bool isMoving = false;

    public bool IsMoving { get => isMoving; set => isMoving = value; }

    public override void UpdateEventRaised(float deltaTime)
	{
        if (!isMoving)
        {
            return;
        }

        transform.position = transform.position +(transform.forward * speed * deltaTime);

	}
}
