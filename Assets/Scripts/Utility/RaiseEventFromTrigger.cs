﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RaiseEventFromTrigger : MonoBehaviour
{
    public LayerMask layerMask;
    public bool oneTime = true;
    public UnityEvent eventToRaise;

    public void OnTriggerEnter(Collider other)
    {
        if (layerMask == (layerMask .value | (1<< other.gameObject.layer)))
        {
            eventToRaise.Invoke();
            if (oneTime)
            {
                Destroy(this);
            }
        }
    }

}
