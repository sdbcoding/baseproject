﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public FloatReference startingHealth;
    int currentHealth;
    int maxHealth;
    // Start is called before the first frame update
    void Start()
    {
        maxHealth = (int)startingHealth.Value;
        currentHealth = maxHealth;
    }

    public void TakeDamage(int amount)
    {
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            Death();
        }
    }
    void Death()
    {
        gameObject.SetActive(false);
    }
}
