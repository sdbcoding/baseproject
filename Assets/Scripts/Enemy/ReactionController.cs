﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class ReactionController : MonoBehaviour {
	public Head enemyHead;
	public Legs enemyMovement;
	//ReactionControls
	public List<Reaction> avaliableReactions;
	[SerializeField]
	Reaction currentReaction;
    public Patrol connectedPatrol;

	//Reaction
	bool isWaiting;
	int index;

	//timer system
	bool isTimerRunning;
	float currentTime;
	float timeToReach;
	Action actionWhenTimerIsDone;

	//Patrol
	void Awake()
	{
		if (avaliableReactions == null)
		{
			avaliableReactions = new List<Reaction>();
		}
		if (avaliableReactions.Count != 0)
		{
			int randomNumber = UnityEngine.Random.Range(0,avaliableReactions.Count);
			currentReaction = avaliableReactions[randomNumber];
			//avaliableReactions.RemoveAt(randomNumber);
		}
	}
	public bool WantControl()
	{
		if (currentReaction == null)
		{
			if(avaliableReactions.Count != 0)
			{
				ChooseNewReaction();
				return true;
			}
			return false;
		}
		return true;
	}

	public void UpdateLoop()
	{
		if (isTimerRunning)
		{
			RunTimer();
		}
		if (!isWaiting)
		{
			if(currentReaction == null)
			{
				isWaiting = false;
			}
			else{
				isWaiting = currentReaction.DoReaction(this,index);
			}
		}
	}
	public void AddNewReaction(Reaction reaction)
	{
		avaliableReactions.Add(reaction);
	}
	public void SetCurrentReation(Reaction reaction)
	{
		ClearCurrentReaction();
		currentReaction = reaction;
	}
	public void ClearCurrentReaction()
	{
		currentReaction = null;
		index = 0;
		isTimerRunning = false;
	}
	void ChooseNewReaction()
	{
		ClearCurrentReaction();
		if (avaliableReactions.Count != 0)
		{
			currentReaction = avaliableReactions[UnityEngine.Random.Range(0,avaliableReactions.Count)];
		}
	}
	void RunTimer()
	{
		currentTime += Time.deltaTime;
		if (currentTime > timeToReach)
		{
			TimerIsFinished();
		}
	}
	void TimerIsFinished()
	{
		actionWhenTimerIsDone.Invoke();
		actionWhenTimerIsDone = null;
		isTimerRunning = false;
	}
	public bool GetIsTimerRunning()
	{
		return isTimerRunning;
	}
	
	public void StartTimer(float time,Action whenDone)
	{
		currentTime = 0;
		timeToReach = time;
		actionWhenTimerIsDone = whenDone;
		isTimerRunning = true;
	}
	public void FinishedReaction()
	{
		isWaiting = false;
		if (currentReaction == null) {return;}
		index = currentReaction.UpdateIndex(index);
		
		if (index == -1)
		{
			ChooseNewReaction();
		}
	}
	public void SetIndex(int newIndex)
	{
		index = newIndex;
	}
}
