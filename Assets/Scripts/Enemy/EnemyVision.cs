﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;

public class EnemyVision : MonoBehaviour {
	public FloatReference visionAngle;

	public FloatReference visionLenght;
	public FloatReference yValue;
	public ThingSet setToLookFor;
	public EnemyMovement enemyMovement;
	public GameEvent thingInSetSpotted;
	List<GameObject> objectsSpotted;
	List<GameObject> objectsSpottedButNoVisible;
    Vector3 spottedObjectPosition;
	bool haveSendMovementCommand;
    public UnityEvent responseWhenThingSetSeen;

	void Awake()
	{
		objectsSpotted = new List<GameObject>();
        objectsSpottedButNoVisible = new List<GameObject>();
    }
	
	void CheckVision()
	{
		//Debug.Log(transform.forward);
		Vector3 forward = transform.forward;
		Collider[] collidersInSphere = Physics.OverlapSphere(transform.position,visionLenght,-1,QueryTriggerInteraction.Collide);
		
		if (collidersInSphere.Length == 0)
		{
			Debug.Log("No colliders within sphere");
			return;
		}
		/* 

		bool shouldShow = false;
		int newI = 0;
		for (int i = 0; i < collidersInSphere.Length; i++)
		{
			if (collidersInSphere[i].gameObject.name == "Player")
			{
				shouldShow = true;
				newI = i;
			}			
		}

		if (shouldShow)
		{
			float angle;
			Vector3 raycastDirection;
            Vector3 yCorrectedSpherePosition = collidersInSphere[newI].transform.position;
            yCorrectedSpherePosition.y = transform.position.y;

            raycastDirection = yCorrectedSpherePosition - transform.position;
			angle = Vector3.Angle(transform.forward,raycastDirection.normalized);
			EnemyDisabler[] disablers = GetComponents<EnemyDisabler>();
			Debug.Log("Player angle: " + angle + " number of Disablers: " + disablers.Length + " objectsSpotted: " + objectsSpotted.Count);
		} else {
			EnemyDisabler[] disablers = GetComponents<EnemyDisabler>();
			Debug.Log("Player not Spotted" + " number of Disablers: " + disablers.Length + " objectsSpotted: " + objectsSpotted.Count);
		}
*/
		objectsSpotted = new List<GameObject>();
        objectsSpottedButNoVisible = new List<GameObject>();

        for (int i = 0; i < collidersInSphere.Length; i++)
		{
			
				//Debug.Log(collidersInSphere[i].gameObject.name);

			//Checks if the found collider is withint th e
			//float angle = Vector3.Angle(transform.forward,(collidersInSphere[i].transform.position -transform.position).normalized);
			float angle;
			Type colliderType = collidersInSphere[i].GetType();
			Vector3 raycastDirection;
            Vector3 yCorrectedSpherePosition = collidersInSphere[i].transform.position;
            yCorrectedSpherePosition.y = transform.position.y;

            raycastDirection = yCorrectedSpherePosition - transform.position;
			angle = Vector3.Angle(transform.forward,raycastDirection.normalized);
				
			//Debug.Log("Angle of Collider; " + angle + " from object: " + collidersInSphere[i].transform.gameObject.name);
			if(Mathf.Abs(angle) > visionAngle.Value /2)
			{
				continue;
			}
			Thing[] thingsOnCollider = collidersInSphere[i].transform.GetComponents<Thing>();
			if (thingsOnCollider.Length == 0)
			{
				continue;
			}
			for (int j = 0; j < thingsOnCollider.Length; j++)
			{
				if (ThingSet.Equals(thingsOnCollider[j].RunTimeSet,setToLookFor))
				{
					//TODO implment a raycast to make sure the player is not behing a wall
					//Debug.Log("Trying for: " + collidersInSphere[i].gameObject.name);
					RaycastHit hit;
					Vector3 newRaycastPosition = transform.position;
					newRaycastPosition.y = 0.5f;
					Physics.Raycast(newRaycastPosition,raycastDirection,out hit,50);

					if (hit.collider == null)
					{
						continue;
					}
					if(GameObject.Equals(hit.collider.gameObject,collidersInSphere[i].gameObject))
					{
						objectsSpotted.Add( collidersInSphere[i].gameObject);
                        objectsSpottedButNoVisible.Add(collidersInSphere[i].gameObject);
                        responseWhenThingSetSeen.Invoke();
                        //Debug.Log("Player spotted: " +  angle);
                    }
                    else {
                        objectsSpottedButNoVisible.Add(collidersInSphere[i].gameObject);

                        //Debug.Log("Player spotted but not in range. Hit: " + hit.collider.gameObject.name + " tried for: " +collidersInSphere[i].gameObject.name);

					}

				}
			}
		}
	}
    public GameObject[] GetobjectsSpottedButNoVisible()
    {

        return objectsSpottedButNoVisible.ToArray();
    }

    public GameObject[] GetSpottedObjects()
	{
		return objectsSpotted.ToArray();
	}

    public bool WantControl()
    {
		CheckVision();
		if (objectsSpotted.Count != 0)
		{
			return true;
		}
		if (haveSendMovementCommand)
		{
			return true;
		}
		return false;
    }

    public void UpdateLoop()
    {
		if (objectsSpotted.Count != 0)
		{
			spottedObjectPosition = objectsSpotted[0].transform.position;
			spottedObjectPosition.y = yValue;
			//SendDestination();
			if (thingInSetSpotted != null)
			{
				thingInSetSpotted.Raise();

			}
		}
    }
	void SendDestination()
	{
		enemyMovement.SetDestination(transform.position,MovementDoneResponse);
		haveSendMovementCommand = true;
	}
	public void MovementDoneResponse()
	{
		haveSendMovementCommand = false;
	}
}
