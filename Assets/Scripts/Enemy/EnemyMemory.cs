﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WeaponNamespace;

public class EnemyMemory : MonoBehaviour
{

    //Eye
    private Vector3 lastKnownPlayerPosition;
    public bool isPlayerVisible;
    [HideInInspector]
    public float timeSincePlayerWasSpotted;
    [HideInInspector]
    public float timeLastKnowPlayerPositionWasUpdated;
    [HideInInspector]
    public float distanceToPlayer;
    [HideInInspector]
    public float angleToPlayer;

    [HideInInspector]
    public float relevantWeaponDistance;

    public Vector3 LastKnownPlayerPosition { get { return lastKnownPlayerPosition; }
        set {
            lastKnownPlayerPosition = value;
            timeLastKnowPlayerPositionWasUpdated = GameTime.ElapsedTime;
            isPlayerVisible = true;
        } }


    public void ReadyForDesireChecks()
    {
        distanceToPlayer = Vector3.Distance(transform.position, LastKnownPlayerPosition);
        timeSincePlayerWasSpotted = GameTime.ElapsedTime - timeLastKnowPlayerPositionWasUpdated;
    }
}
