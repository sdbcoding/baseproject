﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBrain : OnUpdate {

	public EnemyVision enemyVision;
	public ReactionController reactionController;
	public EnemyHearing enemyHearing;
	public string inControl = "";
	bool isRunning = true;
	
	public override void UpdateEventRaised(float deltaTime)
	{
		if (!isRunning) 
		{
			if (enemyVision != null)
			{
			if (enemyVision.WantControl())
				{	
					inControl = "Vision";
					enemyVision.UpdateLoop();
					return;
				}
			}
			return;
		}
		if (enemyVision != null)
		{
			if (enemyVision.WantControl())
			{	
				inControl = "Vision";
				enemyVision.UpdateLoop();
				return;
			}
		}
		if (enemyHearing != null)
		{
			if (enemyHearing.WantControl())
			{
				inControl = "Hearing";

				enemyHearing.UpdateLoop();
				return;

			}
		}
		if (reactionController != null)
		{
			if (reactionController.WantControl())
			{
				inControl = "Reaction Controller";
				reactionController.UpdateLoop();
				return;
			}
		}
		
	}
	public void PauseEnemy()
	{
		isRunning = false;
	}
	public void ResumeEnemy()
	{
		isRunning = true;

	}
}
