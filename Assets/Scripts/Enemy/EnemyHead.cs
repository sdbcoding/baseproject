﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class EnemyHead : OnUpdate {
	public FloatReference maxAngle;
	public Transform enemyBody;
	float currentTime;
	float timeToLookThere;
	float timeLookingThere;
	Vector3 lookAtDirection;
	float angleToTurnTo;
	Action turningDoneResponse;
	bool isLookingThere = false;
	Quaternion startRotation;
	
	bool isFocussing = false;
	bool isNormalBehaviour = true;
	void Awake()
	{
		isNormalBehaviour = true;
	}
	public virtual void ClearResponse(Action formerResponse)
	{
		if (Action.Equals(formerResponse,turningDoneResponse))
		{
			turningDoneResponse = null;
			//Debug.Log("Actions match, canceling");
		} else {
			//Debug.Log("Actions mismatch");

		}
	}

	public virtual void ReturnToNormal()
	{
		angleToTurnTo = Vector3.Angle(transform.worldToLocalMatrix *transform.forward,Vector3.forward);
		isLookingThere = false;
		isNormalBehaviour = true;
	}
	public virtual void LookAt(float angle,float timeUntil, float timeThere, Action responseWhenDone)
	{
		isFocussing = false;

		if (Mathf.Abs(angle) > maxAngle)
		{
			Debug.Log("Angle provided is above maxAngle, canceling");
			angle = maxAngle;
		}
		//TODO Implement yValue;
		float angleOffset = Vector3.Angle(transform.forward,enemyBody.forward);

		//angleToTurnTo = angle + angleOffset;
		angleToTurnTo = angle;
		//Debug.Log(angleToTurnTo +" med angleoffset: " +angleOffset + " transform.forward: " + transform.forward + " enemyBody: " + enemyBody.forward);
		isNormalBehaviour = false;
		currentTime = 0;
		timeToLookThere = timeUntil;
		timeLookingThere = timeThere;
		startRotation = transform.localRotation;
		//Debug.Log("Start rotation: " + startRotation);
		if (turningDoneResponse != null)
		{
			turningDoneResponse.Invoke();
		}
		turningDoneResponse = responseWhenDone;
	}
	public virtual void LookAt(Vector3 direction,float timeUntil, float timeThere, Action responseWhenDone)
	{
		isFocussing = false;
		float angle = Vector3.Angle(Vector3.forward,lookAtDirection);
		if (Mathf.Abs(angle) > maxAngle)
		{
			Debug.Log("Angle provided is above maxAngle, canceling");
			angle = maxAngle;
		}
		if (turningDoneResponse != null)
		{
			turningDoneResponse.Invoke();
		}
		turningDoneResponse = responseWhenDone;
		//TODO Implement yValue;
		lookAtDirection = direction;
		float angleOffset = Vector3.Angle(transform.forward,Vector3.forward);
		angleToTurnTo = angle - angleOffset;
		isNormalBehaviour = false;
		currentTime = 0;
		timeToLookThere = timeUntil;
		timeLookingThere = timeThere;
		startRotation = transform.localRotation;

	}
	void LookingDone()
	{
		//Debug.Log("Looking done");
		ReturnToNormal();
		if (turningDoneResponse != null)
		{
			Action tempAction = turningDoneResponse;
			turningDoneResponse = null;
			tempAction.Invoke();
		}
	}

	public override void UpdateEventRaised(float deltaTime)
	{	
		
		if (isFocussing)
		{
			Focussing();
			return;
		}
		if (isNormalBehaviour)
		{
			NormalBehaviour();
		} else {
			LookTowards();
		}
	}
	void Focussing()
	{
		//find angle, if too large take closest to max.
		float angle = Vector3.Angle( transform.forward,transform.position - lookAtDirection);
		if (angle < -maxAngle)
		{
			angle = -maxAngle;
		} else if (angle > maxAngle) {
			angle = maxAngle;
		}
		transform.localRotation = Quaternion.Lerp(transform.localRotation,Quaternion.AngleAxis(angle,transform.up),Time.deltaTime );
	}
	public virtual void FocusOnPosition(Vector3 position)
	{
		isFocussing = true;
		lookAtDirection = position;
	}
	void LookTowards()
	{
		currentTime += Time.deltaTime;
		if (isLookingThere)
		{
			if (timeLookingThere < currentTime)
			{
				LookingDone();
				//Return to normal?
				return;
			}
		} else {
			//Debug.Log("Looking there");

			transform.localRotation = Quaternion.Lerp(startRotation,Quaternion.AngleAxis(angleToTurnTo,transform.up),currentTime/timeToLookThere);
			
			
			if (timeToLookThere < currentTime)
			{
				isLookingThere = true;
				currentTime = 0;
				return;
			}
		}
	}
	void NormalBehaviour()
	{
		//Wait for game designer
		transform.localRotation = Quaternion.Lerp(transform.localRotation,Quaternion.AngleAxis(angleToTurnTo,transform.up),Time.deltaTime);

	}

	
}
