﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillerSpheresBrain : OnUpdate
{
    public Transform target;
    public KillerSpheresMovement movement;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    public override void UpdateEventRaised(float deltaTime)
    {
        movement.Destination = target.position;
        
    }
}
