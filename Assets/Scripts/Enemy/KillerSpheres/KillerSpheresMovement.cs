﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class KillerSpheresMovement : OnUpdate
{
    public Rigidbody rb;
    private Vector3 destination;
    public float minDistance = 0.5f;
    public float minAngle = 10.0f;
    public float maxMagnitude = 5.0f;
    public float force = 100;
    public float antiGravityForce = 10;
    public float hoverForce = 65;
    public float minDistanceToGround = 0.5f;
    public LayerMask groundLayer;

    int numberOfPreviousDestinations = 25;
    Vector3[] prevDestinations;
    float currentUpdateTime = 0;
    float updateEach = 0.04f;

    public Vector3 Destination { get => destination; set => destination = value; }

    // Start is called before the first frame update
    void Awake()
    {
        if (rb == null)
        {
            rb = GetComponent<Rigidbody>();
        }

        Destination = transform.position;
        prevDestinations = new Vector3[numberOfPreviousDestinations];
    }
    
    void UpdatePrevDestinations(float deltaTime)
    {
        currentUpdateTime += deltaTime;

        int breakNumber = 0;
        while (currentUpdateTime > updateEach)
        {
            currentUpdateTime -= updateEach;
            for (int i =0 ; i < numberOfPreviousDestinations -1; i++)
            {
                prevDestinations[i] = prevDestinations[i + 1];
            }
            prevDestinations[numberOfPreviousDestinations -1] = Destination;

            //While Safety
            breakNumber++;
            if (breakNumber == 10)
            {
                break;
            }
        }
        

        
    }

    public void FaceDirection(float deltaTime)
    {
        Vector3 tempLookPosition = prevDestinations[0];
        tempLookPosition.y = transform.position.y;

        float angle = Vector3.SignedAngle(transform.forward, (transform.position - tempLookPosition).normalized, transform.up);
        if (Mathf.Abs(angle) < 175.0f)
        {
            transform.Rotate(transform.up, -Mathf.Sign(angle) * 85 * deltaTime);
            return;
            if (angle < 0.0f)
            {
                rb.AddTorque(Vector3.left * 50);

            } else
            {
                rb.AddTorque(Vector3.right * 50);

            }
        }
    }

    public override void UpdateEventRaised(float deltaTime)
    {
        UpdatePrevDestinations(deltaTime);
        FaceDirection(deltaTime);
        Hover();
        if (prevDestinations[0].y > transform.position.y + 0.5f) {ReachSameHeightAsTarget();}

        //if distance is too short, go towards it. If angle seems correct and speed is fast enough. Do nothing.
        float distance = Vector3.Distance(transform.position, prevDestinations[0]);

        if (distance < minDistance) {return;}
        
        Vector3 direction = (prevDestinations[0] - transform.position).normalized;

        float angle = Mathf.Abs(Vector3.Angle(rb.velocity.normalized,direction));

        if (angle < minAngle) 
        {
            if (rb.velocity.magnitude > maxMagnitude) {return;}

            //Debug.Log(rb.velocity.magnitude);
        }

        rb.AddForce((RandomizeVector(direction)) * force,ForceMode.Force);
    }

    Vector3 RandomizeVector(Vector3 vector)
    {
        float x = Random.Range(-0.2f,0.2f);
        float y = Random.Range(-0.2f,0.2f);
        float z = Random.Range(-0.2f,0.2f);

        Vector3 newVector = new Vector3(vector.x + x,vector.y + y,vector.z +z);
        return newVector;
    }

    public void Hover()
    {
        Ray ray = new Ray(transform.position,Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit,minDistanceToGround,groundLayer))
        {
            float proportionalHeight = (minDistanceToGround - hit.distance) / minDistanceToGround;
            Vector3 appliedForce = Vector3.up * proportionalHeight * hoverForce;
            rb.AddForce(appliedForce,ForceMode.Acceleration);
        } 

    }

    public void ReachSameHeightAsTarget()
    {
        rb.AddForce(antiGravityForce * Vector3.up,ForceMode.Force);
    }
    
}
