﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchCollider : MonoBehaviour
{
    public GameObject explosion;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        Punchable punchable = other.GetComponent<Punchable>();
        if (punchable != null)
        {
            other.gameObject.SetActive(false);
            GameObject temp = Instantiate(explosion);
            temp.transform.position = other.transform.position;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        Debug.Log(other.gameObject.name);

    }
}
