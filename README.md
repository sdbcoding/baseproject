# Scriptable object based utility AI

Playground for a top down RPG with items, weapon modifications and more.
Built a Utility-based AI system for the enemies where considerations can be added 
and found by a designer within the Unity editor on any prefab enemy.

Contains standard scriptable object components from baseproject.
Play ground for a utility based ai that uses scriptable objects

Also includes a small first person testbed for enemies.
Built in atticipation that my master might include Utility AI